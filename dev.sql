-- --------------------------------------------------------
-- Host:                         127.0.0.1
-- Server version:               5.1.41 - Source distribution
-- Server OS:                    Win32
-- HeidiSQL version:             7.0.0.4053
-- Date/time:                    2014-05-21 02:10:58
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!40014 SET FOREIGN_KEY_CHECKS=0 */;

-- Dumping database structure for penjadwalan_pa
CREATE DATABASE IF NOT EXISTS `penjadwalan_pa` /*!40100 DEFAULT CHARACTER SET latin1 */;
USE `penjadwalan_pa`;


-- Dumping structure for table penjadwalan_pa.tbl_dosen
CREATE TABLE IF NOT EXISTS `tbl_dosen` (
  `dosen_initial` char(5) NOT NULL,
  `dosen_nama` varchar(50) NOT NULL,
  `dosen_gelar_depan` varchar(50) NOT NULL,
  `dosen_gelar_belakang` varchar(50) NOT NULL,
  `dosen_kk` varchar(255) NOT NULL,
  PRIMARY KEY (`dosen_initial`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table penjadwalan_pa.tbl_dosen: ~2 rows (approximately)
/*!40000 ALTER TABLE `tbl_dosen` DISABLE KEYS */;
INSERT INTO `tbl_dosen` (`dosen_initial`, `dosen_nama`, `dosen_gelar_depan`, `dosen_gelar_belakang`, `dosen_kk`) VALUES
	('BY', 'Bayu Hendrajaya', 'Dr.', 'M.T.', 'RPL'),
	('IL', 'Inggriani Liem', 'Dr.', '', 'RPL'),
	('SM', 'Sukrisno Mardianto', '', '', 'RPL'),
	('WD', 'Wikan Danar', 'Dr.', '', 'RPL');
/*!40000 ALTER TABLE `tbl_dosen` ENABLE KEYS */;


-- Dumping structure for table penjadwalan_pa.tbl_jadwal
CREATE TABLE IF NOT EXISTS `tbl_jadwal` (
  `jadwal_id` int(11) NOT NULL AUTO_INCREMENT,
  `jadwal_dosen` char(5) DEFAULT NULL,
  `jadwal_tanggal` date NOT NULL,
  `jadwal_keterangan` varchar(255) DEFAULT NULL,
  `jadwal_jam` time NOT NULL,
  `jadwal_status` varchar(20) DEFAULT NULL,
  `jadwal_ruang_nama_fix` varchar(255) DEFAULT NULL,
  `jadwal_periode` int(11) NOT NULL,
  `jadwal_ruang` int(11) NOT NULL,
  PRIMARY KEY (`jadwal_id`),
  KEY `jadwal_periode` (`jadwal_periode`,`jadwal_ruang`),
  KEY `jadwal_ruang` (`jadwal_ruang`),
  CONSTRAINT `tbl_jadwal_ibfk_1` FOREIGN KEY (`jadwal_ruang`) REFERENCES `tbl_ruang` (`ruang_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `tbl_jadwal_ibfk_2` FOREIGN KEY (`jadwal_periode`) REFERENCES `tbl_periode` (`periode_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

-- Dumping data for table penjadwalan_pa.tbl_jadwal: ~1 rows (approximately)
/*!40000 ALTER TABLE `tbl_jadwal` DISABLE KEYS */;
INSERT INTO `tbl_jadwal` (`jadwal_id`, `jadwal_dosen`, `jadwal_tanggal`, `jadwal_keterangan`, `jadwal_jam`, `jadwal_status`, `jadwal_ruang_nama_fix`, `jadwal_periode`, `jadwal_ruang`) VALUES
	(1, 'BY', '2014-05-20', NULL, '08:00:00', NULL, NULL, 3, 2),
	(2, 'IL', '2014-05-20', NULL, '09:00:00', NULL, NULL, 3, 2);
/*!40000 ALTER TABLE `tbl_jadwal` ENABLE KEYS */;


-- Dumping structure for table penjadwalan_pa.tbl_jadwal_available
CREATE TABLE IF NOT EXISTS `tbl_jadwal_available` (
  `jadwal_available_id` int(11) NOT NULL AUTO_INCREMENT,
  `jadwal_avaiable_date` date NOT NULL,
  `jadwal_available_waktu` time NOT NULL,
  `jadwal_available_dosen` char(5) NOT NULL,
  `jadwal_available_periode` int(11) NOT NULL,
  PRIMARY KEY (`jadwal_available_id`),
  KEY `jadwal_available_dosen` (`jadwal_available_dosen`),
  KEY `jadwal_available_periode` (`jadwal_available_periode`),
  CONSTRAINT `tbl_jadwal_available_ibfk_1` FOREIGN KEY (`jadwal_available_dosen`) REFERENCES `tbl_dosen` (`dosen_initial`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=latin1;

-- Dumping data for table penjadwalan_pa.tbl_jadwal_available: ~1 rows (approximately)
/*!40000 ALTER TABLE `tbl_jadwal_available` DISABLE KEYS */;
INSERT INTO `tbl_jadwal_available` (`jadwal_available_id`, `jadwal_avaiable_date`, `jadwal_available_waktu`, `jadwal_available_dosen`, `jadwal_available_periode`) VALUES
	(1, '2014-05-20', '08:00:00', 'BY', 3),
	(2, '2014-05-20', '09:00:00', 'IL', 3),
	(4, '2014-05-20', '08:00:00', 'IL', 3),
	(5, '2014-05-20', '08:00:00', 'WD', 3),
	(6, '2014-05-20', '08:00:00', 'SM', 3);
/*!40000 ALTER TABLE `tbl_jadwal_available` ENABLE KEYS */;


-- Dumping structure for table penjadwalan_pa.tbl_jadwal_mengajar
CREATE TABLE IF NOT EXISTS `tbl_jadwal_mengajar` (
  `jadwal_kuliah_id` int(11) NOT NULL AUTO_INCREMENT,
  `jadwal_kuliah_tanggal` date NOT NULL,
  `jadwal_kuliah_waktu` time NOT NULL,
  `jadwal_kuliah_dosen` char(5) NOT NULL,
  `jadwal_kuliah_ruang` int(11) NOT NULL,
  PRIMARY KEY (`jadwal_kuliah_id`),
  KEY `jadwal_kuliah_dosen` (`jadwal_kuliah_dosen`,`jadwal_kuliah_ruang`),
  KEY `jadwal_kuliah_ruang` (`jadwal_kuliah_ruang`),
  CONSTRAINT `tbl_jadwal_mengajar_ibfk_1` FOREIGN KEY (`jadwal_kuliah_dosen`) REFERENCES `tbl_dosen` (`dosen_initial`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `tbl_jadwal_mengajar_ibfk_2` FOREIGN KEY (`jadwal_kuliah_ruang`) REFERENCES `tbl_ruang` (`ruang_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table penjadwalan_pa.tbl_jadwal_mengajar: ~0 rows (approximately)
/*!40000 ALTER TABLE `tbl_jadwal_mengajar` DISABLE KEYS */;
/*!40000 ALTER TABLE `tbl_jadwal_mengajar` ENABLE KEYS */;


-- Dumping structure for table penjadwalan_pa.tbl_mahasiswa
CREATE TABLE IF NOT EXISTS `tbl_mahasiswa` (
  `mhs_nim` varchar(10) NOT NULL,
  `mhs_nama` varchar(255) NOT NULL,
  PRIMARY KEY (`mhs_nim`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table penjadwalan_pa.tbl_mahasiswa: ~4 rows (approximately)
/*!40000 ALTER TABLE `tbl_mahasiswa` DISABLE KEYS */;
INSERT INTO `tbl_mahasiswa` (`mhs_nim`, `mhs_nama`) VALUES
	('23513067', 'Budi Achmadi'),
	('23513070', 'Elia Dolaciho Bangun'),
	('23513117', 'Billy Charles Wagey'),
	('23513174', 'Blasius Neri Puspika');
/*!40000 ALTER TABLE `tbl_mahasiswa` ENABLE KEYS */;


-- Dumping structure for table penjadwalan_pa.tbl_periode
CREATE TABLE IF NOT EXISTS `tbl_periode` (
  `periode_id` int(11) NOT NULL AUTO_INCREMENT,
  `periode_semester` tinyint(4) NOT NULL,
  `periode_tahun` smallint(6) NOT NULL,
  `periode_tglawal` date NOT NULL,
  `periode_tglakhir` date NOT NULL,
  `periode_jenis` varchar(20) NOT NULL,
  `periode_template_jam` int(11) NOT NULL,
  PRIMARY KEY (`periode_id`),
  KEY `periode_template_jam` (`periode_template_jam`),
  CONSTRAINT `tbl_periode_ibfk_1` FOREIGN KEY (`periode_template_jam`) REFERENCES `tbl_template_jam` (`template_jam_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

-- Dumping data for table penjadwalan_pa.tbl_periode: ~1 rows (approximately)
/*!40000 ALTER TABLE `tbl_periode` DISABLE KEYS */;
INSERT INTO `tbl_periode` (`periode_id`, `periode_semester`, `periode_tahun`, `periode_tglawal`, `periode_tglakhir`, `periode_jenis`, `periode_template_jam`) VALUES
	(3, 1, 2014, '2014-01-10', '2014-05-30', '', 1);
/*!40000 ALTER TABLE `tbl_periode` ENABLE KEYS */;


-- Dumping structure for table penjadwalan_pa.tbl_proyek_akhir
CREATE TABLE IF NOT EXISTS `tbl_proyek_akhir` (
  `proyek_akhir_id` int(11) NOT NULL AUTO_INCREMENT,
  `proyek_akhir_judul` varchar(255) NOT NULL,
  `proyek_akhir_status` varchar(255) NOT NULL,
  `proyek_akhir_topik` varchar(255) NOT NULL,
  `proyek_akhir_jadwal` int(11) NOT NULL,
  `proyek_akhir_mahasiswa` varchar(10) NOT NULL,
  PRIMARY KEY (`proyek_akhir_id`),
  KEY `proyek_akhir_jadwal` (`proyek_akhir_jadwal`,`proyek_akhir_mahasiswa`),
  KEY `proyek_akhir_mahasiswa` (`proyek_akhir_mahasiswa`),
  CONSTRAINT `tbl_proyek_akhir_ibfk_1` FOREIGN KEY (`proyek_akhir_mahasiswa`) REFERENCES `tbl_mahasiswa` (`mhs_nim`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `tbl_proyek_akhir_ibfk_2` FOREIGN KEY (`proyek_akhir_jadwal`) REFERENCES `tbl_jadwal` (`jadwal_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

-- Dumping data for table penjadwalan_pa.tbl_proyek_akhir: ~0 rows (approximately)
/*!40000 ALTER TABLE `tbl_proyek_akhir` DISABLE KEYS */;
/*!40000 ALTER TABLE `tbl_proyek_akhir` ENABLE KEYS */;


-- Dumping structure for table penjadwalan_pa.tbl_relasi_proyek_akhir_dosen_pembimbing
CREATE TABLE IF NOT EXISTS `tbl_relasi_proyek_akhir_dosen_pembimbing` (
  `pembimbing_proyek_akhir` int(11) NOT NULL,
  `pembimbing_dosen` char(5) NOT NULL,
  `pembimbing_status` varchar(255) NOT NULL,
  PRIMARY KEY (`pembimbing_proyek_akhir`,`pembimbing_dosen`),
  KEY `pembimbing_dosen` (`pembimbing_dosen`),
  CONSTRAINT `tbl_relasi_proyek_akhir_dosen_pembimbing_ibfk_1` FOREIGN KEY (`pembimbing_dosen`) REFERENCES `tbl_dosen` (`dosen_initial`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `tbl_relasi_proyek_akhir_dosen_pembimbing_ibfk_2` FOREIGN KEY (`pembimbing_proyek_akhir`) REFERENCES `tbl_proyek_akhir` (`proyek_akhir_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table penjadwalan_pa.tbl_relasi_proyek_akhir_dosen_pembimbing: ~0 rows (approximately)
/*!40000 ALTER TABLE `tbl_relasi_proyek_akhir_dosen_pembimbing` DISABLE KEYS */;
/*!40000 ALTER TABLE `tbl_relasi_proyek_akhir_dosen_pembimbing` ENABLE KEYS */;


-- Dumping structure for table penjadwalan_pa.tbl_relasi_proyek_akhir_dosen_penguji
CREATE TABLE IF NOT EXISTS `tbl_relasi_proyek_akhir_dosen_penguji` (
  `penguji_proyek_akhir` int(11) NOT NULL,
  `penguji_dosen` char(5) NOT NULL,
  `penguji_status` varchar(255) NOT NULL,
  PRIMARY KEY (`penguji_proyek_akhir`,`penguji_dosen`),
  KEY `penguji_dosen` (`penguji_dosen`),
  CONSTRAINT `tbl_relasi_proyek_akhir_dosen_penguji_ibfk_1` FOREIGN KEY (`penguji_dosen`) REFERENCES `tbl_dosen` (`dosen_initial`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `tbl_relasi_proyek_akhir_dosen_penguji_ibfk_2` FOREIGN KEY (`penguji_proyek_akhir`) REFERENCES `tbl_proyek_akhir` (`proyek_akhir_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table penjadwalan_pa.tbl_relasi_proyek_akhir_dosen_penguji: ~0 rows (approximately)
/*!40000 ALTER TABLE `tbl_relasi_proyek_akhir_dosen_penguji` DISABLE KEYS */;
/*!40000 ALTER TABLE `tbl_relasi_proyek_akhir_dosen_penguji` ENABLE KEYS */;


-- Dumping structure for table penjadwalan_pa.tbl_ruang
CREATE TABLE IF NOT EXISTS `tbl_ruang` (
  `ruang_id` int(11) NOT NULL,
  `ruang_nama` varchar(255) NOT NULL,
  `ruang_kapasitas` int(11) NOT NULL,
  PRIMARY KEY (`ruang_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table penjadwalan_pa.tbl_ruang: ~3 rows (approximately)
/*!40000 ALTER TABLE `tbl_ruang` DISABLE KEYS */;
INSERT INTO `tbl_ruang` (`ruang_id`, `ruang_nama`, `ruang_kapasitas`) VALUES
	(1, 'R.7608', 40),
	(2, 'R.7601', 100),
	(3, 'R.7602', 30);
/*!40000 ALTER TABLE `tbl_ruang` ENABLE KEYS */;


-- Dumping structure for table penjadwalan_pa.tbl_template_jam
CREATE TABLE IF NOT EXISTS `tbl_template_jam` (
  `template_jam_id` int(11) NOT NULL AUTO_INCREMENT,
  `template_jam_nama` varchar(255) NOT NULL,
  `template_jam_awal` time NOT NULL,
  `template_jam_akhir` time NOT NULL,
  `template_jam_istirahat_awal` time NOT NULL,
  `template_jam_istirahat_akhir` time NOT NULL,
  `template_jam_lama_slot` int(11) NOT NULL,
  PRIMARY KEY (`template_jam_id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

-- Dumping data for table penjadwalan_pa.tbl_template_jam: ~1 rows (approximately)
/*!40000 ALTER TABLE `tbl_template_jam` DISABLE KEYS */;
INSERT INTO `tbl_template_jam` (`template_jam_id`, `template_jam_nama`, `template_jam_awal`, `template_jam_akhir`, `template_jam_istirahat_awal`, `template_jam_istirahat_akhir`, `template_jam_lama_slot`) VALUES
	(1, 'Pagi1', '08:00:00', '09:00:00', '00:00:00', '00:00:00', 0);
/*!40000 ALTER TABLE `tbl_template_jam` ENABLE KEYS */;


-- Dumping structure for table penjadwalan_pa.tbl_user
CREATE TABLE IF NOT EXISTS `tbl_user` (
  `user_name` varchar(50) NOT NULL,
  `user_password` varchar(50) NOT NULL,
  `user_role` varchar(50) NOT NULL,
  `user_dosen` char(5) NOT NULL,
  PRIMARY KEY (`user_name`),
  KEY `user_dosen` (`user_dosen`),
  CONSTRAINT `tbl_user_ibfk_1` FOREIGN KEY (`user_dosen`) REFERENCES `tbl_dosen` (`dosen_initial`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table penjadwalan_pa.tbl_user: ~1 rows (approximately)
/*!40000 ALTER TABLE `tbl_user` DISABLE KEYS */;
INSERT INTO `tbl_user` (`user_name`, `user_password`, `user_role`, `user_dosen`) VALUES
	('user1', '0cc175b9c0f1b6a831c399e269772661', 'koordinator', 'BY');
/*!40000 ALTER TABLE `tbl_user` ENABLE KEYS */;
/*!40014 SET FOREIGN_KEY_CHECKS=1 */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
