-- phpMyAdmin SQL Dump
-- version 3.3.9
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: May 25, 2014 at 04:42 AM
-- Server version: 5.5.8
-- PHP Version: 5.3.5

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `penjadwalan_pa`
--

-- --------------------------------------------------------

--
-- Table structure for table `tbl_dosen`
--

DROP TABLE IF EXISTS `tbl_dosen`;
CREATE TABLE IF NOT EXISTS `tbl_dosen` (
  `dosen_initial` char(5) NOT NULL,
  `dosen_nama` varchar(50) NOT NULL,
  `dosen_gelar_depan` varchar(50) NOT NULL,
  `dosen_gelar_belakang` varchar(50) NOT NULL,
  `dosen_kk` varchar(255) NOT NULL,
  PRIMARY KEY (`dosen_initial`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_dosen`
--

INSERT INTO `tbl_dosen` (`dosen_initial`, `dosen_nama`, `dosen_gelar_depan`, `dosen_gelar_belakang`, `dosen_kk`) VALUES
('BY', 'Bayu Hendrajaya', 'Dr.', 'M.T.', 'RPL'),
('IL', 'Inggriani Liem', 'Dr.', '', 'RPL'),
('SAT', 'Fassat', 'Dr.', '', 'DB');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_jadwal`
--

DROP TABLE IF EXISTS `tbl_jadwal`;
CREATE TABLE IF NOT EXISTS `tbl_jadwal` (
  `jadwal_id` int(11) NOT NULL AUTO_INCREMENT,
  `jadwal_dosen` char(5) NOT NULL,
  `jadwal_tanggal` date NOT NULL,
  `jadwal_jam` time NOT NULL,
  `jadwal_keterangan` varchar(255) NOT NULL,
  `jadwal_status` varchar(20) NOT NULL,
  `jadwal_ruang_nama_fix` varchar(255) NOT NULL,
  `jadwal_periode` int(11) NOT NULL,
  `jadwal_ruang` int(11) NOT NULL,
  PRIMARY KEY (`jadwal_id`),
  KEY `jadwal_periode` (`jadwal_periode`,`jadwal_ruang`),
  KEY `jadwal_ruang` (`jadwal_ruang`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

--
-- Dumping data for table `tbl_jadwal`
--


-- --------------------------------------------------------

--
-- Table structure for table `tbl_jadwal_available`
--

DROP TABLE IF EXISTS `tbl_jadwal_available`;
CREATE TABLE IF NOT EXISTS `tbl_jadwal_available` (
  `jadwal_available_id` int(11) NOT NULL AUTO_INCREMENT,
  `jadwal_avaiable_date` date NOT NULL,
  `jadwal_avaiavle_waktu` time NOT NULL,
  `jadwal_available_dosen` char(5) NOT NULL,
  `jadwal_available_periode` int(11) NOT NULL,
  PRIMARY KEY (`jadwal_available_id`),
  KEY `jadwal_available_dosen` (`jadwal_available_dosen`),
  KEY `jadwal_available_periode` (`jadwal_available_periode`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=3 ;

--
-- Dumping data for table `tbl_jadwal_available`
--

INSERT INTO `tbl_jadwal_available` (`jadwal_available_id`, `jadwal_avaiable_date`, `jadwal_avaiavle_waktu`, `jadwal_available_dosen`, `jadwal_available_periode`) VALUES
(1, '2014-05-21', '10:00:00', 'SAT', 3),
(2, '2014-05-30', '08:00:00', 'BY', 3);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_jadwal_mengajar`
--

DROP TABLE IF EXISTS `tbl_jadwal_mengajar`;
CREATE TABLE IF NOT EXISTS `tbl_jadwal_mengajar` (
  `jadwal_kuliah_id` int(11) NOT NULL AUTO_INCREMENT,
  `jadwal_kuliah_tanggal` date NOT NULL,
  `jadwal_kuliah_waktu` time NOT NULL,
  `jadwal_kuliah_dosen` char(5) NOT NULL,
  `jadwal_kuliah_ruang` int(11) NOT NULL,
  PRIMARY KEY (`jadwal_kuliah_id`),
  KEY `jadwal_kuliah_dosen` (`jadwal_kuliah_dosen`,`jadwal_kuliah_ruang`),
  KEY `jadwal_kuliah_ruang` (`jadwal_kuliah_ruang`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

--
-- Dumping data for table `tbl_jadwal_mengajar`
--


-- --------------------------------------------------------

--
-- Table structure for table `tbl_mahasiswa`
--

DROP TABLE IF EXISTS `tbl_mahasiswa`;
CREATE TABLE IF NOT EXISTS `tbl_mahasiswa` (
  `mhs_nim` varchar(10) NOT NULL,
  `mhs_nama` varchar(255) NOT NULL,
  `prodi` varchar(15) NOT NULL,
  `opsi` varchar(25) NOT NULL,
  PRIMARY KEY (`mhs_nim`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_mahasiswa`
--

INSERT INTO `tbl_mahasiswa` (`mhs_nim`, `mhs_nama`, `prodi`, `opsi`) VALUES
('13513250', 'Damian Dannish', 'S1 Informatika', 'Informatika'),
('23513067', 'Budi Achmadi', 'S2 Informatika', 'RPL'),
('23513070', 'Elia Dolaciho Bangun', 'S2 Informatika', 'RPL'),
('23513117', 'Billy Charles Wagey', 'S2 Informatika', 'RPL'),
('23513174', 'Blasius Neri Puspika', 'S2 Informatika', 'RPL');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_periode`
--

DROP TABLE IF EXISTS `tbl_periode`;
CREATE TABLE IF NOT EXISTS `tbl_periode` (
  `periode_id` int(11) NOT NULL AUTO_INCREMENT,
  `periode_semester` tinyint(4) NOT NULL,
  `periode_tahun` tinyint(4) NOT NULL,
  `periode_tglawal` date NOT NULL,
  `periode_tglakhir` date NOT NULL,
  `periode_tglbatas` date NOT NULL,
  `periode_jumlah_pembimbing` int(11) NOT NULL,
  `periode_jumlah_penguji` int(11) NOT NULL,
  `periode_jenis` varchar(20) NOT NULL,
  `periode_template_jam` int(11) NOT NULL,
  PRIMARY KEY (`periode_id`),
  KEY `periode_template_jam` (`periode_template_jam`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=5 ;

--
-- Dumping data for table `tbl_periode`
--

INSERT INTO `tbl_periode` (`periode_id`, `periode_semester`, `periode_tahun`, `periode_tglawal`, `periode_tglakhir`, `periode_tglbatas`, `periode_jumlah_pembimbing`, `periode_jumlah_penguji`, `periode_jenis`, `periode_template_jam`) VALUES
(3, 0, 0, '2014-05-07', '2014-05-29', '2014-05-29', 1, 1, 'S2 - Seminar 1', 1),
(4, 0, 0, '2014-05-01', '2014-05-20', '2014-05-02', 1, 1, 'S2 - Sidang', 1);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_proyek_akhir`
--

DROP TABLE IF EXISTS `tbl_proyek_akhir`;
CREATE TABLE IF NOT EXISTS `tbl_proyek_akhir` (
  `proyek_akhir_id` int(11) NOT NULL AUTO_INCREMENT,
  `proyek_akhir_judul` varchar(255) NOT NULL,
  `proyek_akhir_status` varchar(255) NOT NULL,
  `proyek_akhir_topik` varchar(255) NOT NULL,
  `proyek_akhir_jadwal` int(11) NOT NULL,
  `proyek_akhir_mahasiswa` varchar(10) NOT NULL,
  PRIMARY KEY (`proyek_akhir_id`),
  KEY `proyek_akhir_jadwal` (`proyek_akhir_jadwal`,`proyek_akhir_mahasiswa`),
  KEY `proyek_akhir_mahasiswa` (`proyek_akhir_mahasiswa`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

--
-- Dumping data for table `tbl_proyek_akhir`
--


-- --------------------------------------------------------

--
-- Table structure for table `tbl_relasi_proyek_akhir_dosen_pembimbing`
--

DROP TABLE IF EXISTS `tbl_relasi_proyek_akhir_dosen_pembimbing`;
CREATE TABLE IF NOT EXISTS `tbl_relasi_proyek_akhir_dosen_pembimbing` (
  `pembimbing_proyek_akhir` int(11) NOT NULL,
  `pembimbing_dosen` char(5) NOT NULL,
  `pembimbing_status` varchar(255) NOT NULL,
  PRIMARY KEY (`pembimbing_proyek_akhir`,`pembimbing_dosen`),
  KEY `pembimbing_dosen` (`pembimbing_dosen`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_relasi_proyek_akhir_dosen_pembimbing`
--


-- --------------------------------------------------------

--
-- Table structure for table `tbl_relasi_proyek_akhir_dosen_penguji`
--

DROP TABLE IF EXISTS `tbl_relasi_proyek_akhir_dosen_penguji`;
CREATE TABLE IF NOT EXISTS `tbl_relasi_proyek_akhir_dosen_penguji` (
  `penguji_proyek_akhir` int(11) NOT NULL,
  `penguji_dosen` char(5) NOT NULL,
  `penguji_status` varchar(255) NOT NULL,
  PRIMARY KEY (`penguji_proyek_akhir`,`penguji_dosen`),
  KEY `penguji_dosen` (`penguji_dosen`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_relasi_proyek_akhir_dosen_penguji`
--


-- --------------------------------------------------------

--
-- Table structure for table `tbl_ruang`
--

DROP TABLE IF EXISTS `tbl_ruang`;
CREATE TABLE IF NOT EXISTS `tbl_ruang` (
  `ruang_id` int(11) NOT NULL,
  `ruang_nama` varchar(255) NOT NULL,
  `ruang_kapasitas` int(11) NOT NULL,
  PRIMARY KEY (`ruang_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_ruang`
--

INSERT INTO `tbl_ruang` (`ruang_id`, `ruang_nama`, `ruang_kapasitas`) VALUES
(1, 'R.7608', 40),
(2, 'R.7601', 100),
(3, 'R.7602', 30);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_template_jam`
--

DROP TABLE IF EXISTS `tbl_template_jam`;
CREATE TABLE IF NOT EXISTS `tbl_template_jam` (
  `template_jam_id` int(11) NOT NULL AUTO_INCREMENT,
  `template_jam_nama` varchar(255) NOT NULL,
  `template_jam_awal` time NOT NULL,
  `template_jam_akhir` time NOT NULL,
  `template_jam_istirahat_awal` time NOT NULL,
  `template_jam_istirahat_akhir` time NOT NULL,
  `template_jam_lama_slot` int(11) NOT NULL,
  PRIMARY KEY (`template_jam_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=12 ;

--
-- Dumping data for table `tbl_template_jam`
--

INSERT INTO `tbl_template_jam` (`template_jam_id`, `template_jam_nama`, `template_jam_awal`, `template_jam_akhir`, `template_jam_istirahat_awal`, `template_jam_istirahat_akhir`, `template_jam_lama_slot`) VALUES
(1, 'Template 1', '08:00:00', '17:00:00', '12:00:00', '13:00:00', 60),
(5, 'nama', '07:00:00', '07:00:00', '07:00:00', '00:00:00', 30),
(7, 'test', '07:00:00', '17:00:00', '12:00:00', '00:00:00', 30),
(8, 'lalala', '12:00:00', '16:00:00', '11:00:00', '00:00:00', 30),
(9, 'test234', '07:00:00', '17:00:00', '12:00:00', '00:00:00', 30),
(10, 'test234', '07:00:00', '17:00:00', '12:00:00', '00:00:00', 30),
(11, 'Temp1', '08:00:00', '16:00:00', '12:00:00', '00:00:00', 60);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_user`
--

DROP TABLE IF EXISTS `tbl_user`;
CREATE TABLE IF NOT EXISTS `tbl_user` (
  `user_name` varchar(50) NOT NULL,
  `user_password` varchar(50) NOT NULL,
  `user_role` varchar(50) NOT NULL,
  `user_dosen` char(5) NOT NULL,
  PRIMARY KEY (`user_name`),
  KEY `user_dosen` (`user_dosen`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_user`
--

INSERT INTO `tbl_user` (`user_name`, `user_password`, `user_role`, `user_dosen`) VALUES
('BY', '0cc175b9c0f1b6a831c399e269772661', 'Dosen', 'BY'),
('Koordinator', 'e10adc3949ba59abbe56e057f20f883e', 'Koordinator', 'SAT');

--
-- Constraints for dumped tables
--

--
-- Constraints for table `tbl_jadwal`
--
ALTER TABLE `tbl_jadwal`
  ADD CONSTRAINT `tbl_jadwal_ibfk_1` FOREIGN KEY (`jadwal_ruang`) REFERENCES `tbl_ruang` (`ruang_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `tbl_jadwal_ibfk_2` FOREIGN KEY (`jadwal_periode`) REFERENCES `tbl_periode` (`periode_id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `tbl_jadwal_available`
--
ALTER TABLE `tbl_jadwal_available`
  ADD CONSTRAINT `tbl_jadwal_available_ibfk_1` FOREIGN KEY (`jadwal_available_dosen`) REFERENCES `tbl_dosen` (`dosen_initial`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `tbl_jadwal_mengajar`
--
ALTER TABLE `tbl_jadwal_mengajar`
  ADD CONSTRAINT `tbl_jadwal_mengajar_ibfk_1` FOREIGN KEY (`jadwal_kuliah_dosen`) REFERENCES `tbl_dosen` (`dosen_initial`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `tbl_jadwal_mengajar_ibfk_2` FOREIGN KEY (`jadwal_kuliah_ruang`) REFERENCES `tbl_ruang` (`ruang_id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `tbl_periode`
--
ALTER TABLE `tbl_periode`
  ADD CONSTRAINT `tbl_periode_ibfk_1` FOREIGN KEY (`periode_template_jam`) REFERENCES `tbl_template_jam` (`template_jam_id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `tbl_proyek_akhir`
--
ALTER TABLE `tbl_proyek_akhir`
  ADD CONSTRAINT `tbl_proyek_akhir_ibfk_1` FOREIGN KEY (`proyek_akhir_mahasiswa`) REFERENCES `tbl_mahasiswa` (`mhs_nim`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `tbl_proyek_akhir_ibfk_2` FOREIGN KEY (`proyek_akhir_jadwal`) REFERENCES `tbl_jadwal` (`jadwal_id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `tbl_relasi_proyek_akhir_dosen_pembimbing`
--
ALTER TABLE `tbl_relasi_proyek_akhir_dosen_pembimbing`
  ADD CONSTRAINT `tbl_relasi_proyek_akhir_dosen_pembimbing_ibfk_1` FOREIGN KEY (`pembimbing_dosen`) REFERENCES `tbl_dosen` (`dosen_initial`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `tbl_relasi_proyek_akhir_dosen_pembimbing_ibfk_2` FOREIGN KEY (`pembimbing_proyek_akhir`) REFERENCES `tbl_proyek_akhir` (`proyek_akhir_id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `tbl_relasi_proyek_akhir_dosen_penguji`
--
ALTER TABLE `tbl_relasi_proyek_akhir_dosen_penguji`
  ADD CONSTRAINT `tbl_relasi_proyek_akhir_dosen_penguji_ibfk_1` FOREIGN KEY (`penguji_dosen`) REFERENCES `tbl_dosen` (`dosen_initial`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `tbl_relasi_proyek_akhir_dosen_penguji_ibfk_2` FOREIGN KEY (`penguji_proyek_akhir`) REFERENCES `tbl_proyek_akhir` (`proyek_akhir_id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `tbl_user`
--
ALTER TABLE `tbl_user`
  ADD CONSTRAINT `tbl_user_ibfk_1` FOREIGN KEY (`user_dosen`) REFERENCES `tbl_dosen` (`dosen_initial`) ON DELETE CASCADE ON UPDATE CASCADE;
