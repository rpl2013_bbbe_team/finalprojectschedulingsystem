/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package WS;

import com.bbbe.Model.Dosen;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

/**
 *
 * @author beleey
 */
public class DosenWS {
    
    private List<Dosen> dosenList;    
    
    public DosenWS() {
        
    }
    
    public List<Dosen> getDosenAll() throws IOException {
        List<Dosen> dsnList = new ArrayList<Dosen>();
        String dosenAll = JSONfunction.getJSON("http://localhost/WebServiceKaryaAkhir/index.php?ret=11", 30);
        try{
                 JSONParser parser=new JSONParser();
                 Object obj = parser.parse(dosenAll.toString());
                 JSONArray array = (JSONArray)obj;
                 int a = array.size();
                 int b = 0;
                 
                 while (b <a ){
                    JSONObject obj2 = (JSONObject)array.get(b);
                    System.out.println(obj2.get("NIP")+"-"+obj2.get("namaDosen"));    
                    Dosen dn = new Dosen(b + "", obj2.get("namaDosen")+"", obj2.get("NIP")+"","","");
                    dsnList.add(dn);
                    b++;
                 }

              }catch(ParseException pe){
                 System.out.println("position: " + pe.getPosition());
                 System.out.println(pe);
              }
        return dsnList;
    }
}
