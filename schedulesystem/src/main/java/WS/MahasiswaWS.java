/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package WS;


import com.bbbe.Model.Mahasiswa;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

/**
 *
 * @author beleey
 */
public class MahasiswaWS {
    private List<Mahasiswa> mhsList; 
    
    public List<Mahasiswa> getMahasiswaAll() throws IOException {
        List<Mahasiswa> mhsList = new ArrayList<Mahasiswa>();
        String mahasiswaAll = JSONfunction.getJSON("http://localhost/WebServiceKaryaAkhir/index.php?ret=14", 30);
        try{
                 JSONParser parser=new JSONParser();
                 Object obj = parser.parse(mahasiswaAll.toString());
                 JSONArray array = (JSONArray)obj;
                 int a = array.size();
                 int b = 0;
                 
                 while (b <a ){
                    JSONObject obj2 = (JSONObject)array.get(b);
                    Mahasiswa m = new Mahasiswa(obj2.get("NIM")+"", obj2.get("namaMhs")+"","","");
                    mhsList.add(m);
                    b++;
                 }

              }catch(ParseException pe){
                 System.out.println("position: " + pe.getPosition());
                 System.out.println(pe);
              }
        return mhsList;
    }
}
