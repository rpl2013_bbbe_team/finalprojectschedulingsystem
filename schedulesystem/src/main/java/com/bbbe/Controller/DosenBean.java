/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.bbbe.Controller;

import WS.DosenWS;
import com.bbbe.Dao.DosenDao;
import com.bbbe.JpaDao.DosenDaoImpl;
import com.bbbe.Model.Dosen;
import java.io.IOException;
import java.io.Serializable;
import java.util.List;
import javax.ejb.EJB;
import javax.enterprise.context.SessionScoped;
import javax.faces.bean.ManagedBean;

/**
 *
 * @author SONY
 */
@ManagedBean(name ="dosenBean",eager = true)
@SessionScoped
public class DosenBean implements Serializable{
    @EJB(name = "dosenDao")
    private DosenDaoImpl dosenDao = new DosenDaoImpl();
    private List<Dosen> dosenList;
    private Dosen dosen;
    private String kkDosen;
    
    public DosenBean() {
    }
    
    public List<Dosen> getDosenList() {
     //    dosenList =  ContDosen.getAllDosen();
       //  dosenList =  dosenDao.findAll();
        return  dosenList;
    }

    public Dosen getDosen() {
        return dosen;
    }
    
    public void setDosen(Dosen dosen) {
        this.dosen = dosen;
    }

    public void setDosenDao(DosenDao dosenDao) {
        this.dosenDao = (DosenDaoImpl) dosenDao;
    }

    public void setDosenList(List<Dosen> dosenList) {
        this.dosenList = dosenList;
    }

    public void setKkDosen(String kkDosen) {
        this.kkDosen = kkDosen;
    }
    
    public List<Dosen> getDosenListByKK( String KK) {
         dosenList =  dosenDao.findDosenByKK(KK);
        return  dosenList;
    }
    
    public List<Dosen> getAllDosenList() throws IOException {
        //dosenList =  dosenDao.findAll();
        DosenWS dws = new DosenWS();
        dosenList = dws.getDosenAll();
        return  dosenList;
    }

    public String getKkDosen() {
        return kkDosen;
    }
    
    
    
}
