/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.bbbe.Controller;

import com.bbbe.Dao.JadwalDao;
import com.bbbe.JpaDao.JadwalDaoImpl;
import com.bbbe.Model.Jadwal;
import java.io.Serializable;
import java.util.List;
import javax.ejb.EJB;
import javax.enterprise.context.SessionScoped;
import javax.faces.bean.ManagedBean;

/**
 *
 * @author SONY
 */
@ManagedBean(name ="jadwalBean",eager = true)
@SessionScoped
public class JadwalBean implements Serializable{
    @EJB(name = "jadwalDao")
    private JadwalDaoImpl jadwalDao = new JadwalDaoImpl();
    private List<Jadwal> jadwalList;
    private Jadwal jadwal;
    private String kkJadwal;
    
    public JadwalBean() {
    }
    
    public List<Jadwal> getJadwalList() {
     //    jadwalList =  ContJadwal.getAllJadwal();
         jadwalList =  jadwalDao.findAll();
        return  jadwalList;
    }

    public Jadwal getJadwal() {
        return jadwal;
    }
    
    public void setJadwal(Jadwal jadwal) {
        this.jadwal = jadwal;
    }

    public void setJadwalDao(JadwalDao jadwalDao) {
        this.jadwalDao = (JadwalDaoImpl) jadwalDao;
    }

    public void setJadwalList(List<Jadwal> jadwalList) {
        this.jadwalList = jadwalList;
    }

    
    public List<Jadwal> getAllJadwalList() {
         jadwalList =  jadwalDao.findAll();
        return  jadwalList;
    }

    public String getKkJadwal() {
        return kkJadwal;
    }
    
    
    
}
