/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.bbbe.Controller;

import WS.DosenWS;
import com.bbbe.Dao.UserDao;
import com.bbbe.JpaDao.UserDaoImpl;
import com.bbbe.Model.User;
import com.bbbe.generator.Cross;
import java.io.IOException;
import java.io.Serializable;
import java.security.MessageDigest;
import java.util.List;
import javax.ejb.EJB;
import javax.enterprise.context.SessionScoped;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.context.FacesContext;


/**
 *
 * @author SONY
 */

@ManagedBean(name ="loginBean",eager = true)
@SessionScoped
public class LoginBean implements Serializable{

  private String username;
  private String password;
  private boolean loggedIn;
  @ManagedProperty(value="#{navigationBean}")
  private NavigationBean navigationBean;
  
    @EJB(name = "userDao")
    private UserDao userDao = new UserDaoImpl();
    private List<User> userList;
  
    public LoginBean() {
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
    
    public String doLogin() {
        String Textpassword2MD5 = MD5(password);

        getUserList();
        for (User user: userList) {
            String dbUsername = user.getUserName();
            String dbPassword = user.getUserPassword();
            String dbRole = user.getUserRole();
System.out.println(" ");
System.out.println(password+" "+Textpassword2MD5+" "+dbPassword);
System.out.println(" ");
            if (dbUsername.equalsIgnoreCase(username) && dbPassword.equals(Textpassword2MD5)) {
                loggedIn = true;
                    if(dbRole.equalsIgnoreCase("koordinator")){
                       return navigationBean.redirecttoKoordinator(); 
                    } else if (dbRole.equalsIgnoreCase("dosen")){
                        return navigationBean.redirecttoKoordinator();
                    }
                    else if (dbRole.equalsIgnoreCase("mahasiswa")){
                        return navigationBean.redirecttoKoordinator();
                    }
                
            }
        }
         
        // Set login ERROR
        FacesMessage msg = new FacesMessage("Login error!", "ERROR MSG");
        msg.setSeverity(FacesMessage.SEVERITY_ERROR);
        FacesContext.getCurrentInstance().addMessage(null, msg);
         
        // To to login page
        return navigationBean.toLogin();
         
    }
    
    public String doLogout() {
        // Set the paremeter indicating that user is logged in to false
        loggedIn = false;
         
        // Set logout message
        FacesMessage msg = new FacesMessage("Logout success!", "INFO MSG");
        msg.setSeverity(FacesMessage.SEVERITY_INFO);
        FacesContext.getCurrentInstance().addMessage(null, msg);
         
        return navigationBean.toLogin();
    }
 
    public boolean isLoggedIn() {
        return loggedIn;
    }
 
    public void setLoggedIn(boolean loggedIn) {
        this.loggedIn = loggedIn;
    }
 
    public void setNavigationBean(NavigationBean navigationBean) {
        this.navigationBean = navigationBean;
    }

    public UserDao getUserDao() {
        return userDao;
    }

    public void setUserDao(UserDaoImpl userDao) {
        this.userDao = userDao;
    }

    public List<User> getUserList() {
        userList = userDao.findAll();
        return userList;
    }

    public void setUserList(List<User> userList) {
        this.userList = userList;
    }

public String MD5(String md5) {
   try {
        java.security.MessageDigest md = java.security.MessageDigest.getInstance("MD5");
        byte[] array = md.digest(md5.getBytes());
        StringBuffer sb = new StringBuffer();
        for (int i = 0; i < array.length; ++i) {
          sb.append(Integer.toHexString((array[i] & 0xFF) | 0x100).substring(1,3));
       }
        return sb.toString();
    } catch (java.security.NoSuchAlgorithmException e) {
    }
    return null;
}
    
public void generator_dump() {

    System.out.println("\n\ngenerator_dump() \n\n");
         Cross objCross = new Cross();
         objCross.JDBCExample();
    }

    public void tampilkanDosen() throws IOException {
        DosenWS dws = new DosenWS();
        dws.getDosenAll();
    }
}

