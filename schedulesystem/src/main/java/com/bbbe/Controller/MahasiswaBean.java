/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.bbbe.Controller;

import WS.MahasiswaWS;
import com.bbbe.Dao.MahasiswaDao;
import com.bbbe.JpaDao.MahasiswaDaoImpl;
import com.bbbe.Model.Mahasiswa;
import java.io.IOException;
import javax.enterprise.context.SessionScoped;
import java.io.Serializable;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.ejb.EJB;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.context.FacesContext;

/**
 *
 * @author SONY
 */
@ManagedBean(name ="mahasiswaBean",eager = true)
@SessionScoped
public class MahasiswaBean implements Serializable {
    @EJB(name = "mahasiswaDao")
    private MahasiswaDao mahasiswaDao = new MahasiswaDaoImpl();
    private List<Mahasiswa> mahasiswaList;
    private Mahasiswa mahasiswa;
    
    private Map<String,Map<String,String>> data = new HashMap<String, Map<String,String>>();

    
    
    private String prodi; 

    public String getProdi() {
        return prodi;
    }

    public void setProdi(String prodi) {
        this.prodi = prodi;
    }

    public String getOpsi() {
        return opsi;
    }

    public void setOpsi(String opsi) {
        this.opsi = opsi;
    }

    public Map<String, String> getListProdi() {
        return listProdi;
    }

    public void setListProdi(Map<String, String> listProdi) {
        this.listProdi = listProdi;
    }

    public Map<String, String> getListOpsi() {
        return listOpsi;
    }

    public void setListOpsi(Map<String, String> listOpsi) {
        this.listOpsi = listOpsi;
    }
    private String opsi;  
    private Map<String,String> listProdi;
    private Map<String,String> listOpsi;
    
    /**
     * Creates a new instance of MahasiswaBean
     */
    public MahasiswaBean() {
        listProdi  = new HashMap<String, String>();
        listProdi.put("S1 Informatika", "S1 Informatika");
        listProdi.put("S2 Informatika", "S2 Informatika");
         
        Map<String,String> map = new HashMap<String, String>();
        map.put("Informatika", "Informatika");
        data.put("S1 Informatika", map);
         
        map = new HashMap<String, String>();
        map.put("Rekaya Perangkat Lunak","Rekaya Perangkat Lunak");
        map.put("Ilmu Komputer","Ilmu Komputer");
        map.put("Sistem Informasi","Sistem Informasi");
        map.put("Bussines Intelligent","Bussines Intelligent");
        map.put("High Processing Computer","High Processing Computer");
        map.put("Media Digital dan Game","Media Digital dan Game");
        map.put("Teknologi Informatika","Teknologi Informatika");
        map.put("Keamanan Komputer","Keamanan Komputer");
        map.put("Sistem Cerdas","Sistem Cerdas");
        data.put("S2 Informatika", map);
    }
    
    public Map<String, Map<String, String>> getData() {
        return data;
    }
    
    
    public void onProdiChange() {
        if(prodi !=null && !prodi.equals(""))
            listProdi = data.get(prodi);
        else
            listProdi = new HashMap<String, String>();
    }
     
    public void displayOpsi() {
        FacesMessage msg;
        if(opsi != null && prodi != null)
            msg = new FacesMessage("Selected", opsi + " of " + prodi);
        else
            msg = new FacesMessage(FacesMessage.SEVERITY_ERROR, "Invalid", "City is not selected."); 
             
        FacesContext.getCurrentInstance().addMessage(null, msg);  
    }
    
    public MahasiswaDao getMahasiswaDao() {
        return mahasiswaDao;
    }

    public void setMahasiswaDao(MahasiswaDaoImpl mahasiswaDao) {
        this.mahasiswaDao = mahasiswaDao;
    }

    public List<Mahasiswa> getMahasiswaList() throws IOException {
        //mahasiswaList = mahasiswaDao.findAll();
        MahasiswaWS mws = new MahasiswaWS();
        mahasiswaList =  mws.getMahasiswaAll();
        return mahasiswaList;
    }

    public void setMahasiswaList(List<Mahasiswa> mahasiswaList) {
        this.mahasiswaList = mahasiswaList;
    }

    public Mahasiswa getMahasiswa() {
        return mahasiswa;
    }

    public void setMahasiswa(Mahasiswa mahasiswa) {
        this.mahasiswa = mahasiswa;
    }
    
    
    
}
