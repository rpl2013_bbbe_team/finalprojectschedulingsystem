/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.bbbe.Controller;

import com.bbbe.Dao.PeriodeDao;
import com.bbbe.JpaDao.PeriodeDaoImpl;
import com.bbbe.Model.Periode;
import java.io.Serializable;
import java.util.List;
import javax.ejb.EJB;
import javax.enterprise.context.SessionScoped;
import javax.faces.bean.ManagedBean;

/**
 *
 * @author SONY
 */
@ManagedBean(name ="periodeBean",eager = true)
@SessionScoped
public class PeriodeBean implements Serializable {
    @EJB(name = "periodeDao")
    private PeriodeDao periodeDao = new PeriodeDaoImpl();
    private List<Periode> periodeList;
    private Periode periode = new Periode();
    
    public PeriodeBean() {
    }

    public PeriodeDao getPeriodeDao() {
        return periodeDao;
    }

    public void setPeriodeDao(PeriodeDao periodeDao) {
        this.periodeDao = periodeDao;
    }

    public List<Periode> getPeriodeList() {
        periodeList = periodeDao.findAll();
        return periodeList;
    }

    public void setPeriodeList(List<Periode> periodeList) {
        this.periodeList = periodeList;
    }

    public Periode getPeriode() {
        return periode;
    }

    public void setPeriode(Periode periode) {
        this.periode = periode;
    }
    
    public void savePeriode(Periode periode)
    {
        periodeDao.save(periode);
    }
   
}
