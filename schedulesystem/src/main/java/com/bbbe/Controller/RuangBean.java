/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.bbbe.Controller;

import com.bbbe.Dao.RuangDao;
import com.bbbe.JpaDao.RuangDaoImpl;
import com.bbbe.Model.Ruang;
import java.io.Serializable;
import java.util.List;
import javax.ejb.EJB;
import javax.enterprise.context.SessionScoped;
import javax.faces.bean.ManagedBean;

/**
 *
 * @author SONY
 */
@ManagedBean(name ="ruangBean",eager = true)
@SessionScoped
public class RuangBean implements Serializable {
    @EJB(name = "ruangDao")
    private RuangDao ruangDao = new RuangDaoImpl();
    private List<Ruang> ruangList;
    private List<Ruang> ruangListNama;
    private Ruang ruang;
    /**
     * Creates a new instance of RuangBean
     */
    public RuangBean() {
        
    }
    
    public List<Ruang> getRuangList() {
        ruangList =  ruangDao.findAll();
        return  ruangList;
    }
    
    public List<Ruang> getRuangListNama(String ruangNama) {
        ruangListNama =  ruangDao.findRuangNama(ruangNama);
        return  ruangListNama;
    }

    public Ruang getRuang() {
        return ruang;
    }
    
    public void setRuang(Ruang ruang) {
        this.ruang = ruang;
    }
}
