/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.bbbe.Controller;

import com.bbbe.Dao.TemplateJamDao;
import com.bbbe.JpaDao.TemplateJamDaoImpl;
import com.bbbe.Model.TemplateJam;
import java.io.Serializable;
import java.sql.Time;
import java.util.Date;
import java.util.List;
import javax.ejb.EJB;
import javax.enterprise.context.SessionScoped;
import javax.faces.bean.ManagedBean;

/**
 *
 * @author SONY
 */
@ManagedBean(name ="templateJamBean",eager = true)
@SessionScoped
public class TemplateJamBean implements Serializable{
    @EJB(name = "templatejamDao")
    private TemplateJamDao templatejamDao = new TemplateJamDaoImpl();
    private List<TemplateJam> templateJamList;
    private TemplateJam templateJam = new TemplateJam();
    private int templateJamAwal;
    private int templateJamAkhir;
    private int templateJamIstirahatAwal;
    private int templateJamIstirahatAkhir;
    
    
    /**
     * Creates a new instance of TemplateJamBean
     */
    public TemplateJamBean() {
    }

    public TemplateJamDao getTemplatejamDao() {
        return templatejamDao;
    }

    public void setTemplatejamDao(TemplateJamDao templatejamDao) {
        this.templatejamDao = templatejamDao;
    }

    public List<TemplateJam> getTemplateJamList() {
         templateJamList = templatejamDao.findAll();
        return templateJamList;
    }

    public void setTemplateJamList(List<TemplateJam> templateJamList) {
        this.templateJamList = templateJamList;
    }

    public TemplateJam getTemplateJam() {
        return templateJam;
    }
    
    public void saveTemplateJam (TemplateJam templateJam)
    {
        templateJam.setTemplateJamAwal(InttoTime(templateJamAwal));
        templateJam.setTemplateJamAkhir(InttoTime(templateJamAkhir));
        templateJam.setTemplateJamIstirahatAwal(InttoTime(templateJamIstirahatAwal));
        templateJam.setTemplateJamIstirahatAkhir(InttoTime(templateJamIstirahatAkhir));
        templatejamDao.save(templateJam);
    }

    
    public Date InttoTime(int date)
    {
        Date ActualDate = new Time(0, 0, 0);
        
        switch (date) {
            case 7:  ActualDate = new Time(7, 0, 0);
                     break;
            case 8:  ActualDate = new Time(8, 0, 0);
                     break;
            case 9:  ActualDate = new Time(9, 0, 0);
                     break;
            case 10:  ActualDate = new Time(10, 0, 0);
                     break;
            case 11:  ActualDate = new Time(11, 0, 0);
                     break;
            case 12:  ActualDate = new Time(12, 0, 0);
                     break;
            case 13:  ActualDate = new Time(13, 0, 0);
                     break;
            case 14:  ActualDate = new Time(14, 0, 0);
                     break;
            case 15:  ActualDate = new Time(15, 0, 0);
                     break;
            case 16: ActualDate = new Time(16, 0, 0);
                     break;
            case 17: ActualDate = new Time(17, 0, 0);
                     break;
            default: ActualDate = new Time(0, 0, 0);
                     break;
        }
        
        return ActualDate;
    }

    public void setTemplateJam(TemplateJam templateJam) {
        this.templateJam = templateJam;
    }

    public int getTemplateJamAwal() {
        return templateJamAwal;
    }

    public void setTemplateJamAwal(int templateJamAwal) {
        this.templateJamAwal = templateJamAwal;
    }

    public int getTemplateJamAkhir() {
        return templateJamAkhir;
    }

    public void setTemplateJamAkhir(int templateJamAkhir) {
        this.templateJamAkhir = templateJamAkhir;
    }

    public int getTemplateJamIstirahatAwal() {
        return templateJamIstirahatAwal;
    }

    public void setTemplateJamIstirahatAwal(int templateJamIstirahatAwal) {
        this.templateJamIstirahatAwal = templateJamIstirahatAwal;
    }

    public int getTemplateJamIstirahatAkhir() {
        return templateJamIstirahatAkhir;
    }

    public void setTemplateJamIstirahatAkhir(int templateJamIstirahatAkhir) {
        this.templateJamIstirahatAkhir = templateJamIstirahatAkhir;
    }
    
    
}
