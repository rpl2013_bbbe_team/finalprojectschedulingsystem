/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.bbbe.Converter;

import com.bbbe.Dao.PeriodeDao;
import com.bbbe.JpaDao.PeriodeDaoImpl;
import com.bbbe.Model.Periode;
import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;

/**
 *
 * @author SONY
 */
@ManagedBean(name = "periodeConverter")
public class PeriodeConverter implements Converter{
    @EJB
    private PeriodeDao periodeDAO = new PeriodeDaoImpl();
    
    public PeriodeConverter() {
    }

    @Override
    public Periode getAsObject(FacesContext fc, UIComponent uic, String value) {
        return periodeDAO.find(Integer.valueOf(value));
    }

    @Override
    public String getAsString(FacesContext fc, UIComponent uic, Object o) {
         return ((Periode) o).getPeriodeId().toString();
    }
    
}