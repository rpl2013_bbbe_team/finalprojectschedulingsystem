/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.bbbe.Converter;

import com.bbbe.Dao.TemplateJamDao;
import com.bbbe.JpaDao.TemplateJamDaoImpl;
import com.bbbe.Model.TemplateJam;
import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;

/**
 *
 * @author SONY
 */
@ManagedBean(name = "templateJamConverter")
public class TemplateJamConverter implements Converter{
    @EJB
    private TemplateJamDao templateJamDAO = new TemplateJamDaoImpl();
    
    public TemplateJamConverter() {
    }

    @Override
    public TemplateJam getAsObject(FacesContext fc, UIComponent uic, String value) {
        return templateJamDAO.find(Integer.valueOf(value));
    }

    @Override
    public String getAsString(FacesContext fc, UIComponent uic, Object o) {
         return ((TemplateJam) o).getTemplateJamId().toString();
    }
    
}