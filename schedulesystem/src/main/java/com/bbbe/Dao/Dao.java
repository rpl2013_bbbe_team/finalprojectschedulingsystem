package com.bbbe.Dao;

import java.io.Serializable;
import java.util.List;

/**
 *
 * @author BBBE
 */
public interface Dao<E , K > {
    List<E> findAll();
    
    E find(K id);
    
    void save(E e);
    
    E update(E e);
    
    void delete(E e);
}
