package com.bbbe.Dao;
 
import com.bbbe.Model.Dosen;
import javax.ejb.Remote;

/**
 *
 * @author BBBE
 */
@Remote
public interface DosenDao extends Dao<Dosen, String> {
}
