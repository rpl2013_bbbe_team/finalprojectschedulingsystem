package com.bbbe.Dao;
 
import com.bbbe.Model.Periode;
import javax.ejb.Remote;

/**
 *
 * @author BBBE
 */
@Remote
public interface PeriodeDao extends Dao<Periode, Integer> {
}
