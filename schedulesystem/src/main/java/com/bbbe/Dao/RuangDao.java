/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.bbbe.Dao;

import com.bbbe.Model.Ruang;
import java.util.List;

/**
 *
 * @author SONY
 */
public interface RuangDao extends Dao<Ruang, Integer>{

    public List<Ruang> findRuangNama(String ruangNama);
    
}
