/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.bbbe.Dao;

import com.bbbe.Model.User;
import javax.ejb.Remote;

/**
 *
 * @author BBBE
 */
@Remote
public interface UserDao extends Dao<User, String>{
    
}
