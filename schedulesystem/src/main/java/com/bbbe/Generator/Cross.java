/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.bbbe.generator;

import com.bbbe.Model.JadwalAvailable;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Budi Jati achmadi <budi_jati@yahoo.com>
 */
//public class Cross implements Generator{
public class Cross {

//    @Override
    public void generate() {
 
    }
    
    private void JadwalAvailabel(){
           String JDBC_DRIVER = "com.mysql.jdbc.Driver";  
           String DB_URL = "jdbc:mysql://localhost:3306/penjadwalan_pa?zeroDateTimeBehavior=convertToNull";
           String USER = "penjadwalan";
           String PASS = "j4dw4l";
           Connection conn = null;
           Statement stmt = null;
           Statement stmtInsert = null;
           Statement stmtRuang = null;

           try{
              Class.forName("com.mysql.jdbc.Driver");
              conn = DriverManager.getConnection(DB_URL, USER, PASS);
              stmt = conn.createStatement();
              stmtInsert = conn.createStatement();
              stmtRuang = conn.createStatement();
              
              stmt.execute("TRUNCATE `tbl_jadwal`"); // empty table
              ResultSet rs_ruang = stmtRuang.executeQuery("SELECT * FROM tbl_ruang");
              rs_ruang.next();
              int kapasitas_ruang = 1;
              
              String sql = "SELECT * FROM tbl_jadwal_available order by jadwal_avaiable_date, jadwal_available_waktu ASC";
              ResultSet rs = stmt.executeQuery(sql);
              
              while(rs.next()){
                 String a_date = rs.getString("jadwal_avaiable_date");
                 String a_waktu = rs.getString("jadwal_available_waktu");
                 String a_periode = rs.getString("jadwal_available_periode");                
                 String a_dosen = rs.getString("jadwal_available_dosen");
                 if(kapasitas_ruang <= 3){
                     kapasitas_ruang++;
                 }else{
                     if(rs_ruang.last()){
                         rs_ruang.first(); 
                     }
                     rs_ruang.next();
                     kapasitas_ruang = 1;
                 }
                 String sqlInsert = "INSERT INTO `tbl_jadwal` (`jadwal_tanggal`, `jadwal_jam`, `jadwal_periode`, `jadwal_ruang_nama_fix`, `jadwal_ruang`, `jadwal_dosen`) VALUES ('"+
                         a_date+ "', '"+a_waktu+"', "+a_periode+",'"+rs_ruang.getString("ruang_nama")+"',"+rs_ruang.getInt("ruang_id")+" , '"+a_dosen+"')";
System.out.println(sqlInsert+"\n\n");

                 stmtInsert.execute(sqlInsert);
              }
              rs.close();
           }catch(SQLException se){
              //Handle errors for JDBC
              se.printStackTrace();
           }catch(Exception e){
              //Handle errors for Class.forName
              e.printStackTrace();
           }finally{
              //finally block used to close resources
              try{
                 if(stmt!=null)
                    conn.close();
              }catch(SQLException se){
              }// do nothing
              try{
                 if(conn!=null)
                    conn.close();
              }catch(SQLException se){
                 se.printStackTrace();
              }//end finally try
           }//end try       
    }
    
    public void JDBCExample() {
        /*
           // JDBC driver name and database URL
           String JDBC_DRIVER = "com.mysql.jdbc.Driver";  
           String DB_URL = "jdbc:mysql://localhost:3306/penjadwalan_pa?zeroDateTimeBehavior=convertToNull";

           //  Database credentials
           String USER = "penjadwalan";
           String PASS = "j4dw4l";


           Connection conn = null;
           Statement stmt = null;
           try{
              //STEP 2: Register JDBC driver
              Class.forName("com.mysql.jdbc.Driver");

              //STEP 3: Open a connection
              System.out.println("Connecting to a selected database...");
              conn = DriverManager.getConnection(DB_URL, USER, PASS);
              System.out.println("Connected database successfully...");

              //STEP 4: Execute a query
              System.out.println("Creating statement...");
              stmt = conn.createStatement();

              String sql = "SELECT * FROM tbl_dosen";
              ResultSet rs = stmt.executeQuery(sql);
              //STEP 5: Extract data from result set
              while(rs.next()){
                 //Retrieve by column name
                 String first = rs.getString(3);
                 String last = rs.getString(4);

                 //Display values
                 System.out.print("ID: " + rs.getString(1));
                 System.out.print(", Age: " + rs.getString(2));
                 System.out.print(", First: " + first);
                 System.out.println(", Last: " + last);
              }
              rs.close();
           }catch(SQLException se){
              //Handle errors for JDBC
              se.printStackTrace();
           }catch(Exception e){
              //Handle errors for Class.forName
              e.printStackTrace();
           }finally{
              //finally block used to close resources
              try{
                 if(stmt!=null)
                    conn.close();
              }catch(SQLException se){
              }// do nothing
              try{
                 if(conn!=null)
                    conn.close();
              }catch(SQLException se){
                 se.printStackTrace();
              }//end finally try
           }//end try
                */
           JadwalAvailabel();
           System.out.println("Goodbye!");
                   
    }//end JDBCExample    
        
}