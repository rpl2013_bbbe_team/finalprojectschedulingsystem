/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.bbbe.Generator;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.URL;
import java.net.URLConnection;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import com.bbbe.Generator.mMahasiswa;

/**
 *
 * @author Budi Jati achmadi <budi_jati@yahoo.com>
 */
public class ServiceMahasiswa {

    public void ServiceMhs() throws IOException{
            URL oracle = null;
            StringBuffer sbf = new StringBuffer();
            try {
                oracle = new URL("http://localhost/webservice/index.php?ret=14");
                URLConnection yc = oracle.openConnection();
                BufferedReader in = new BufferedReader(new InputStreamReader(
                        yc.getInputStream()));
                String inputLine;
                while ((inputLine = in.readLine()) != null){
                    System.out.println(inputLine);
                    sbf.append(inputLine);
                }
                in.close();
                System.out.println("\nsbf\n"+sbf.toString());
            
                 JSONParser parser=new JSONParser();
                 Object obj = parser.parse(sbf.toString());
                 JSONArray array = (JSONArray)obj;
                 int a = array.size();
                 int b = 0;
                 mMahasiswa objMhs = new mMahasiswa();
                 objMhs.EmptyMahasiswa();
                 while (b <a ){
                    JSONObject obj2 = (JSONObject)array.get(b);
                    System.out.println(obj2.get("NIM")+"-"+obj2.get("namaMhs"));  
                    objMhs.InsertMahasiswa(obj2.get("NIM").toString(), obj2.get("namaMhs").toString());
                    b++;
                 }

              }catch(ParseException pe){
                 System.out.println("position: " + pe.getPosition());
                 System.out.println(pe);
              }
    
    }    
    
}
