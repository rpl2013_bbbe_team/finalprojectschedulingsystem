package com.bbbe.JpaDao;


import com.bbbe.Dao.DosenDao;
import com.bbbe.Model.Dosen;
import java.util.List;
import javax.ejb.Stateless;

/**
 *
 * @author BBBE
 */
@Stateless
public class DosenDaoImpl extends JpaDao<Dosen, String>
implements DosenDao {
    public DosenDaoImpl() {
    }
    
    public List<Dosen> findDosenByKK( String KK)
    {
        javax.persistence.Query query = em.createQuery("SELECT d FROM Dosen d WHERE d.dosenKk LIKE :KK");
        query.setParameter("KK", "%" +KK+"%");
        List <Dosen> findList = query.getResultList();
        return findList;
    }
    
}
