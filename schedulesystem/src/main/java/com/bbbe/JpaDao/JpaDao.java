package com.bbbe.JpaDao;

import com.bbbe.Dao.Dao;
import java.lang.reflect.ParameterizedType;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.EntityTransaction;
import javax.persistence.Persistence;
import javax.persistence.PersistenceContext;

/**
 *
 * @author BBBE
 */
public abstract class JpaDao<E, K> implements Dao<E, K> {
    
    private Class<E> entityClass;
    private EntityManagerFactory emf = Persistence.createEntityManagerFactory("com.BBBE_PenjadwalanProjectAkhir_war_1.0PU");
    //@PersistenceContext(unitName="com.BBBE_SistemPenjadwalanProyekAkhir_war_1.0PU")
    protected EntityManager em = emf.createEntityManager();
    
    public JpaDao() {
        ParameterizedType genericSuperClass = (ParameterizedType) getClass().getGenericSuperclass();
        entityClass = (Class<E>) genericSuperClass.getActualTypeArguments()[0];
    }
    
    public void setEntityManager(EntityManager em) {
        this.em = em;
    }
    
    public EntityManager getEntityManager() {
        return em;
    }
    
    @Override
    public List<E> findAll() {
        StringBuilder sb = new StringBuilder()
            .append("select e from ")
            .append(entityClass.getName())
            .append(" e");
        List<E> findList = em.createQuery(sb.toString()).getResultList();
        return findList;
    }
    
    @Override
    public E find(K id) {
        return em.find(entityClass, id);
    }
    
    @Override
    public void save(E e) {
        EntityTransaction tx = em.getTransaction();
        tx.begin();
        em.persist(e);
        tx.commit();
    }
    
    @Override
    public E update(E e) {
        return em.merge(e);
    }
    
    @Override
    public void delete(E e) {
        em.remove(e);
    }
}
