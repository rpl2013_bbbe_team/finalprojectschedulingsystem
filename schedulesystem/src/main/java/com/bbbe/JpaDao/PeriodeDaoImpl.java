package com.bbbe.JpaDao;


import com.bbbe.Dao.PeriodeDao;
import com.bbbe.Model.Periode;
import javax.ejb.Stateless;

/**
 *
 * @author BBBE
 */
@Stateless
public class PeriodeDaoImpl extends JpaDao<Periode, Integer>
implements PeriodeDao {
    public PeriodeDaoImpl() {
    }
    
}
