/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.bbbe.JpaDao;

import com.bbbe.Dao.RuangDao;
import com.bbbe.Model.Ruang;
import java.util.List;

/**
 *
 * @author SONY
 */
public class RuangDaoImpl extends JpaDao<Ruang, Integer>
implements RuangDao{
    
    public  RuangDaoImpl()
    {    
        }
    
    public List<Ruang> findRuangNama( String Nama)
    {
        javax.persistence.Query query = em.createQuery("SELECT r FROM Ruang r WHERE r.ruangNama LIKE :ID");
        query.setParameter("ID", "%" +Nama+"%");
        List <Ruang> findList = query.getResultList();
        return findList;
    }
    
}
