/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.bbbe.Model;

import java.io.Serializable;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author SONY
 */
@Entity
@Table(name = "tbl_dosen")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Dosen.findAll", query = "SELECT d FROM Dosen d"),
    @NamedQuery(name = "Dosen.findByDosenInitial", query = "SELECT d FROM Dosen d WHERE d.dosenInitial = :dosenInitial"),
    @NamedQuery(name = "Dosen.findByDosenNama", query = "SELECT d FROM Dosen d WHERE d.dosenNama = :dosenNama"),
    @NamedQuery(name = "Dosen.findByDosenGelarDepan", query = "SELECT d FROM Dosen d WHERE d.dosenGelarDepan = :dosenGelarDepan"),
    @NamedQuery(name = "Dosen.findByDosenGelarBelakang", query = "SELECT d FROM Dosen d WHERE d.dosenGelarBelakang = :dosenGelarBelakang"),
    @NamedQuery(name = "Dosen.findByDosenKk", query = "SELECT d FROM Dosen d WHERE d.dosenKk = :dosenKk")})
public class Dosen implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 5)
    @Column(name = "dosen_initial")
    private String dosenInitial;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 5)
    @Column(name = "dosen_nama")
    private String dosenNama;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 50)
    @Column(name = "dosen_gelar_depan")
    private String dosenGelarDepan;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 50)
    @Column(name = "dosen_gelar_belakang")
    private String dosenGelarBelakang;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 255)
    @Column(name = "dosen_kk")
    private String dosenKk;
    @ManyToMany(mappedBy = "dosenList")
    private List<ProyekAkhir> proyekAkhirList;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "dosen")
    private List<RelasiProyekAkhirDosenPenguji> relasiProyekAkhirDosenPengujiList;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "userDosen")
    private List<User> userList;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "jadwalKuliahDosen")
    private List<JadwalMengajar> jadwalMengajarList;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "jadwalAvailableDosen")
    private List<JadwalAvailable> jadwalAvailableList;

    public Dosen() {
    }

    public Dosen(String dosenInitial) {
        this.dosenInitial = dosenInitial;
    }

    public Dosen(String dosenInitial, String dosenNama, String dosenGelarDepan, String dosenGelarBelakang, String dosenKk) {
        this.dosenInitial = dosenInitial;
        this.dosenNama = dosenNama;
        this.dosenGelarDepan = dosenGelarDepan;
        this.dosenGelarBelakang = dosenGelarBelakang;
        this.dosenKk = dosenKk;
    }

    public String getDosenInitial() {
        return dosenInitial;
    }

    public void setDosenInitial(String dosenInitial) {
        this.dosenInitial = dosenInitial;
    }

    public String getDosenNama() {
        return dosenNama;
    }

    public void setDosenNama(String dosenNama) {
        this.dosenNama = dosenNama;
    }

    public String getDosenGelarDepan() {
        return dosenGelarDepan;
    }

    public void setDosenGelarDepan(String dosenGelarDepan) {
        this.dosenGelarDepan = dosenGelarDepan;
    }

    public String getDosenGelarBelakang() {
        return dosenGelarBelakang;
    }

    public void setDosenGelarBelakang(String dosenGelarBelakang) {
        this.dosenGelarBelakang = dosenGelarBelakang;
    }

    public String getDosenKk() {
        return dosenKk;
    }

    public void setDosenKk(String dosenKk) {
        this.dosenKk = dosenKk;
    }

    @XmlTransient
    public List<ProyekAkhir> getProyekAkhirList() {
        return proyekAkhirList;
    }

    public void setProyekAkhirList(List<ProyekAkhir> proyekAkhirList) {
        this.proyekAkhirList = proyekAkhirList;
    }

    @XmlTransient
    public List<RelasiProyekAkhirDosenPenguji> getRelasiProyekAkhirDosenPengujiList() {
        return relasiProyekAkhirDosenPengujiList;
    }

    public void setRelasiProyekAkhirDosenPengujiList(List<RelasiProyekAkhirDosenPenguji> relasiProyekAkhirDosenPengujiList) {
        this.relasiProyekAkhirDosenPengujiList = relasiProyekAkhirDosenPengujiList;
    }

    @XmlTransient
    public List<User> getUserList() {
        return userList;
    }

    public void setUserList(List<User> userList) {
        this.userList = userList;
    }

    @XmlTransient
    public List<JadwalMengajar> getJadwalMengajarList() {
        return jadwalMengajarList;
    }

    public void setJadwalMengajarList(List<JadwalMengajar> jadwalMengajarList) {
        this.jadwalMengajarList = jadwalMengajarList;
    }

    @XmlTransient
    public List<JadwalAvailable> getJadwalAvailableList() {
        return jadwalAvailableList;
    }

    public void setJadwalAvailableList(List<JadwalAvailable> jadwalAvailableList) {
        this.jadwalAvailableList = jadwalAvailableList;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (dosenInitial != null ? dosenInitial.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Dosen)) {
            return false;
        }
        Dosen other = (Dosen) object;
        if ((this.dosenInitial == null && other.dosenInitial != null) || (this.dosenInitial != null && !this.dosenInitial.equals(other.dosenInitial))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.bbbe.Model.Dosen[ dosenInitial=" + dosenInitial + " ]";
    }
    
}
