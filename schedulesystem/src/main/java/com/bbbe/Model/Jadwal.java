/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.bbbe.Model;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Budi Jati achmadi <budi_jati@yahoo.com>
 */
@Entity
@Table(name = "tbl_jadwal")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Jadwal.findAll", query = "SELECT j FROM Jadwal j"),
    @NamedQuery(name = "Jadwal.findByJadwalId", query = "SELECT j FROM Jadwal j WHERE j.jadwalId = :jadwalId"),
    @NamedQuery(name = "Jadwal.findByJadwalDosen", query = "SELECT j FROM Jadwal j WHERE j.jadwalDosen = :jadwalDosen"),
    @NamedQuery(name = "Jadwal.findByJadwalTanggal", query = "SELECT j FROM Jadwal j WHERE j.jadwalTanggal = :jadwalTanggal"),
    @NamedQuery(name = "Jadwal.findByJadwalKeterangan", query = "SELECT j FROM Jadwal j WHERE j.jadwalKeterangan = :jadwalKeterangan"),
    @NamedQuery(name = "Jadwal.findByJadwalJam", query = "SELECT j FROM Jadwal j WHERE j.jadwalJam = :jadwalJam"),
    @NamedQuery(name = "Jadwal.findByJadwalStatus", query = "SELECT j FROM Jadwal j WHERE j.jadwalStatus = :jadwalStatus"),
    @NamedQuery(name = "Jadwal.findByJadwalRuangNamaFix", query = "SELECT j FROM Jadwal j WHERE j.jadwalRuangNamaFix = :jadwalRuangNamaFix")})
public class Jadwal implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "jadwal_id")
    private Integer jadwalId;
    @Size(max = 5)
    @Column(name = "jadwal_dosen")
    private String jadwalDosen;
    @Basic(optional = false)
    @NotNull
    @Column(name = "jadwal_tanggal")
    @Temporal(TemporalType.DATE)
    private Date jadwalTanggal;
    @Size(max = 255)
    @Column(name = "jadwal_keterangan")
    private String jadwalKeterangan;
    @Basic(optional = false)
    @NotNull
    @Column(name = "jadwal_jam")
    @Temporal(TemporalType.TIME)
    private Date jadwalJam;
    @Size(max = 20)
    @Column(name = "jadwal_status")
    private String jadwalStatus;
    @Size(max = 255)
    @Column(name = "jadwal_ruang_nama_fix")
    private String jadwalRuangNamaFix;
    @JoinColumn(name = "jadwal_periode", referencedColumnName = "periode_id")
    @ManyToOne(optional = false)
    private Periode jadwalPeriode;
    @JoinColumn(name = "jadwal_ruang", referencedColumnName = "ruang_id")
    @ManyToOne(optional = false)
    private Ruang jadwalRuang;

    public Jadwal() {
    }

    public Jadwal(Integer jadwalId) {
        this.jadwalId = jadwalId;
    }

    public Jadwal(Integer jadwalId, Date jadwalTanggal, Date jadwalJam) {
        this.jadwalId = jadwalId;
        this.jadwalTanggal = jadwalTanggal;
        this.jadwalJam = jadwalJam;
    }

    public Integer getJadwalId() {
        return jadwalId;
    }

    public void setJadwalId(Integer jadwalId) {
        this.jadwalId = jadwalId;
    }

    public String getJadwalDosen() {
        return jadwalDosen;
    }

    public void setJadwalDosen(String jadwalDosen) {
        this.jadwalDosen = jadwalDosen;
    }

    public Date getJadwalTanggal() {
        return jadwalTanggal;
    }

    public void setJadwalTanggal(Date jadwalTanggal) {
        this.jadwalTanggal = jadwalTanggal;
    }

    public String getJadwalKeterangan() {
        return jadwalKeterangan;
    }

    public void setJadwalKeterangan(String jadwalKeterangan) {
        this.jadwalKeterangan = jadwalKeterangan;
    }

    public Date getJadwalJam() {
        return jadwalJam;
    }

    public void setJadwalJam(Date jadwalJam) {
        this.jadwalJam = jadwalJam;
    }

    public String getJadwalStatus() {
        return jadwalStatus;
    }

    public void setJadwalStatus(String jadwalStatus) {
        this.jadwalStatus = jadwalStatus;
    }

    public String getJadwalRuangNamaFix() {
        return jadwalRuangNamaFix;
    }

    public void setJadwalRuangNamaFix(String jadwalRuangNamaFix) {
        this.jadwalRuangNamaFix = jadwalRuangNamaFix;
    }

    public Periode getJadwalPeriode() {
        return jadwalPeriode;
    }

    public void setJadwalPeriode(Periode jadwalPeriode) {
        this.jadwalPeriode = jadwalPeriode;
    }

    public Ruang getJadwalRuang() {
        return jadwalRuang;
    }

    public void setJadwalRuang(Ruang jadwalRuang) {
        this.jadwalRuang = jadwalRuang;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (jadwalId != null ? jadwalId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Jadwal)) {
            return false;
        }
        Jadwal other = (Jadwal) object;
        if ((this.jadwalId == null && other.jadwalId != null) || (this.jadwalId != null && !this.jadwalId.equals(other.jadwalId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.bbbe.Model.Jadwal[ jadwalId=" + jadwalId + " ]";
    }
    
}
