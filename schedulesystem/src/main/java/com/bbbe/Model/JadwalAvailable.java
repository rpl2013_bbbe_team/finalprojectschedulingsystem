/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.bbbe.Model;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author SONY
 */
@Entity
@Table(name = "tbl_jadwal_available")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "JadwalAvailable.findAll", query = "SELECT j FROM JadwalAvailable j"),
    @NamedQuery(name = "JadwalAvailable.findByJadwalAvailableId", query = "SELECT j FROM JadwalAvailable j WHERE j.jadwalAvailableId = :jadwalAvailableId"),
    @NamedQuery(name = "JadwalAvailable.findByJadwalAvaiableDate", query = "SELECT j FROM JadwalAvailable j WHERE j.jadwalAvaiableDate = :jadwalAvaiableDate"),
    @NamedQuery(name = "JadwalAvailable.findByJadwalAvaiavleWaktu", query = "SELECT j FROM JadwalAvailable j WHERE j.jadwalAvaiavleWaktu = :jadwalAvaiavleWaktu")})
public class JadwalAvailable implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "jadwal_available_id")
    private Integer jadwalAvailableId;
    @Basic(optional = false)
    @NotNull
    @Column(name = "jadwal_avaiable_date")
    @Temporal(TemporalType.DATE)
    private Date jadwalAvaiableDate;
    @Basic(optional = false)
    @NotNull
    @Column(name = "jadwal_avaiavle_waktu")
    @Temporal(TemporalType.TIME)
    private Date jadwalAvaiavleWaktu;
    @JoinColumn(name = "jadwal_available_dosen", referencedColumnName = "dosen_initial")
    @ManyToOne(optional = false)
    private Dosen jadwalAvailableDosen;

    public JadwalAvailable() {
    }

    public JadwalAvailable(Integer jadwalAvailableId) {
        this.jadwalAvailableId = jadwalAvailableId;
    }

    public JadwalAvailable(Integer jadwalAvailableId, Date jadwalAvaiableDate, Date jadwalAvaiavleWaktu) {
        this.jadwalAvailableId = jadwalAvailableId;
        this.jadwalAvaiableDate = jadwalAvaiableDate;
        this.jadwalAvaiavleWaktu = jadwalAvaiavleWaktu;
    }

    public Integer getJadwalAvailableId() {
        return jadwalAvailableId;
    }

    public void setJadwalAvailableId(Integer jadwalAvailableId) {
        this.jadwalAvailableId = jadwalAvailableId;
    }

    public Date getJadwalAvaiableDate() {
        return jadwalAvaiableDate;
    }

    public void setJadwalAvaiableDate(Date jadwalAvaiableDate) {
        this.jadwalAvaiableDate = jadwalAvaiableDate;
    }

    public Date getJadwalAvaiavleWaktu() {
        return jadwalAvaiavleWaktu;
    }

    public void setJadwalAvaiavleWaktu(Date jadwalAvaiavleWaktu) {
        this.jadwalAvaiavleWaktu = jadwalAvaiavleWaktu;
    }

    public Dosen getJadwalAvailableDosen() {
        return jadwalAvailableDosen;
    }

    public void setJadwalAvailableDosen(Dosen jadwalAvailableDosen) {
        this.jadwalAvailableDosen = jadwalAvailableDosen;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (jadwalAvailableId != null ? jadwalAvailableId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof JadwalAvailable)) {
            return false;
        }
        JadwalAvailable other = (JadwalAvailable) object;
        if ((this.jadwalAvailableId == null && other.jadwalAvailableId != null) || (this.jadwalAvailableId != null && !this.jadwalAvailableId.equals(other.jadwalAvailableId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.bbbe.Model.JadwalAvailable[ jadwalAvailableId=" + jadwalAvailableId + " ]";
    }
    
}
