/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.bbbe.Model;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author SONY
 */
@Entity
@Table(name = "tbl_jadwal_mengajar")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "JadwalMengajar.findAll", query = "SELECT j FROM JadwalMengajar j"),
    @NamedQuery(name = "JadwalMengajar.findByJadwalKuliahId", query = "SELECT j FROM JadwalMengajar j WHERE j.jadwalKuliahId = :jadwalKuliahId"),
    @NamedQuery(name = "JadwalMengajar.findByJadwalKuliahTanggal", query = "SELECT j FROM JadwalMengajar j WHERE j.jadwalKuliahTanggal = :jadwalKuliahTanggal"),
    @NamedQuery(name = "JadwalMengajar.findByJadwalKuliahWaktu", query = "SELECT j FROM JadwalMengajar j WHERE j.jadwalKuliahWaktu = :jadwalKuliahWaktu")})
public class JadwalMengajar implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "jadwal_kuliah_id")
    private Integer jadwalKuliahId;
    @Basic(optional = false)
    @NotNull
    @Column(name = "jadwal_kuliah_tanggal")
    @Temporal(TemporalType.DATE)
    private Date jadwalKuliahTanggal;
    @Basic(optional = false)
    @NotNull
    @Column(name = "jadwal_kuliah_waktu")
    @Temporal(TemporalType.TIME)
    private Date jadwalKuliahWaktu;
    @JoinColumn(name = "jadwal_kuliah_dosen", referencedColumnName = "dosen_initial")
    @ManyToOne(optional = false)
    private Dosen jadwalKuliahDosen;
    @JoinColumn(name = "jadwal_kuliah_ruang", referencedColumnName = "ruang_id")
    @ManyToOne(optional = false)
    private Ruang jadwalKuliahRuang;

    public JadwalMengajar() {
    }

    public JadwalMengajar(Integer jadwalKuliahId) {
        this.jadwalKuliahId = jadwalKuliahId;
    }

    public JadwalMengajar(Integer jadwalKuliahId, Date jadwalKuliahTanggal, Date jadwalKuliahWaktu) {
        this.jadwalKuliahId = jadwalKuliahId;
        this.jadwalKuliahTanggal = jadwalKuliahTanggal;
        this.jadwalKuliahWaktu = jadwalKuliahWaktu;
    }

    public Integer getJadwalKuliahId() {
        return jadwalKuliahId;
    }

    public void setJadwalKuliahId(Integer jadwalKuliahId) {
        this.jadwalKuliahId = jadwalKuliahId;
    }

    public Date getJadwalKuliahTanggal() {
        return jadwalKuliahTanggal;
    }

    public void setJadwalKuliahTanggal(Date jadwalKuliahTanggal) {
        this.jadwalKuliahTanggal = jadwalKuliahTanggal;
    }

    public Date getJadwalKuliahWaktu() {
        return jadwalKuliahWaktu;
    }

    public void setJadwalKuliahWaktu(Date jadwalKuliahWaktu) {
        this.jadwalKuliahWaktu = jadwalKuliahWaktu;
    }

    public Dosen getJadwalKuliahDosen() {
        return jadwalKuliahDosen;
    }

    public void setJadwalKuliahDosen(Dosen jadwalKuliahDosen) {
        this.jadwalKuliahDosen = jadwalKuliahDosen;
    }

    public Ruang getJadwalKuliahRuang() {
        return jadwalKuliahRuang;
    }

    public void setJadwalKuliahRuang(Ruang jadwalKuliahRuang) {
        this.jadwalKuliahRuang = jadwalKuliahRuang;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (jadwalKuliahId != null ? jadwalKuliahId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof JadwalMengajar)) {
            return false;
        }
        JadwalMengajar other = (JadwalMengajar) object;
        if ((this.jadwalKuliahId == null && other.jadwalKuliahId != null) || (this.jadwalKuliahId != null && !this.jadwalKuliahId.equals(other.jadwalKuliahId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.bbbe.Model.JadwalMengajar[ jadwalKuliahId=" + jadwalKuliahId + " ]";
    }
    
}
