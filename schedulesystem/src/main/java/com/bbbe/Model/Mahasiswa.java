/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.bbbe.Model;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author beleey
 */
@Entity
@Table(name = "tbl_mahasiswa")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Mahasiswa.findAll", query = "SELECT m FROM Mahasiswa m"),
    @NamedQuery(name = "Mahasiswa.findByMhsNim", query = "SELECT m FROM Mahasiswa m WHERE m.mhsNim = :mhsNim"),
    @NamedQuery(name = "Mahasiswa.findByMhsNama", query = "SELECT m FROM Mahasiswa m WHERE m.mhsNama = :mhsNama"),
    @NamedQuery(name = "Mahasiswa.findByProdi", query = "SELECT m FROM Mahasiswa m WHERE m.prodi = :prodi"),
    @NamedQuery(name = "Mahasiswa.findByOpsi", query = "SELECT m FROM Mahasiswa m WHERE m.opsi = :opsi")})
public class Mahasiswa implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 10)
    @Column(name = "mhs_nim")
    private String mhsNim;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 255)
    @Column(name = "mhs_nama")
    private String mhsNama;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 15)
    @Column(name = "prodi")
    private String prodi;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 25)
    @Column(name = "opsi")
    private String opsi;

    public Mahasiswa() {
    }

    public Mahasiswa(String mhsNim) {
        this.mhsNim = mhsNim;
    }

    public Mahasiswa(String mhsNim, String mhsNama, String prodi, String opsi) {
        this.mhsNim = mhsNim;
        this.mhsNama = mhsNama;
        this.prodi = prodi;
        this.opsi = opsi;
    }

    public String getMhsNim() {
        return mhsNim;
    }

    public void setMhsNim(String mhsNim) {
        this.mhsNim = mhsNim;
    }

    public String getMhsNama() {
        return mhsNama;
    }

    public void setMhsNama(String mhsNama) {
        this.mhsNama = mhsNama;
    }

    public String getProdi() {
        return prodi;
    }

    public void setProdi(String prodi) {
        this.prodi = prodi;
    }

    public String getOpsi() {
        return opsi;
    }

    public void setOpsi(String opsi) {
        this.opsi = opsi;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (mhsNim != null ? mhsNim.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Mahasiswa)) {
            return false;
        }
        Mahasiswa other = (Mahasiswa) object;
        if ((this.mhsNim == null && other.mhsNim != null) || (this.mhsNim != null && !this.mhsNim.equals(other.mhsNim))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.bbbe.Model.Mahasiswa[ mhsNim=" + mhsNim + " ]";
    }
    
}
