/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.bbbe.Model;

import java.io.Serializable;
import java.util.Date;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author SONY
 */
@Entity
@Table(name = "tbl_periode")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Periode.findAll", query = "SELECT p FROM Periode p"),
    @NamedQuery(name = "Periode.findByPeriodeId", query = "SELECT p FROM Periode p WHERE p.periodeId = :periodeId"),
    @NamedQuery(name = "Periode.findByPeriodeSemester", query = "SELECT p FROM Periode p WHERE p.periodeSemester = :periodeSemester"),
    @NamedQuery(name = "Periode.findByPeriodeTahun", query = "SELECT p FROM Periode p WHERE p.periodeTahun = :periodeTahun"),
    @NamedQuery(name = "Periode.findByPeriodeTglawal", query = "SELECT p FROM Periode p WHERE p.periodeTglawal = :periodeTglawal"),
    @NamedQuery(name = "Periode.findByPeriodeTglakhir", query = "SELECT p FROM Periode p WHERE p.periodeTglakhir = :periodeTglakhir"),
    @NamedQuery(name = "Periode.findByPeriodeJenis", query = "SELECT p FROM Periode p WHERE p.periodeJenis = :periodeJenis")})
public class Periode implements Serializable {
    @Column(name = "periode_jumlah_pembimbing")
    private Integer periodeJumlahPembimbing;
    @JoinColumn(name = "periode_template_jam", referencedColumnName = "template_jam_id")
    @ManyToOne(optional = false)
    private TemplateJam periodeTemplateJam;
    @Basic(optional = false)
    @NotNull
    @Column(name = "periode_tglbatas")
    @Temporal(TemporalType.DATE)
    private Date periodeTglbatas;
    @Basic(optional = false)
    @NotNull
    @Column(name = "periode_jumlah_penguji")
    private int periodeJumlahPenguji;
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "periode_id")
    private Integer periodeId;
    @Basic(optional = false)
    @NotNull
    @Column(name = "periode_semester")
    private short periodeSemester;
    @Basic(optional = false)
    @NotNull
    @Column(name = "periode_tahun")
    private short periodeTahun;
    @Basic(optional = false)
    @NotNull
    @Column(name = "periode_tglawal")
    @Temporal(TemporalType.DATE)
    private Date periodeTglawal;
    @Basic(optional = false)
    @NotNull
    @Column(name = "periode_tglakhir")
    @Temporal(TemporalType.DATE)
    private Date periodeTglakhir;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 20)
    @Column(name = "periode_jenis")
    private String periodeJenis;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "jadwalPeriode")
    private List<Jadwal> jadwalList;

    public Periode() {
    }

    public Periode(Integer periodeId) {
        this.periodeId = periodeId;
    }

    public Periode(Integer periodeId, short periodeSemester, short periodeTahun, Date periodeTglawal, Date periodeTglakhir, String periodeJenis) {
        this.periodeId = periodeId;
        this.periodeSemester = periodeSemester;
        this.periodeTahun = periodeTahun;
        this.periodeTglawal = periodeTglawal;
        this.periodeTglakhir = periodeTglakhir;
        this.periodeJenis = periodeJenis;
    }

    public Integer getPeriodeId() {
        return periodeId;
    }

    public void setPeriodeId(Integer periodeId) {
        this.periodeId = periodeId;
    }

    public short getPeriodeSemester() {
        return periodeSemester;
    }

    public void setPeriodeSemester(short periodeSemester) {
        this.periodeSemester = periodeSemester;
    }

    public short getPeriodeTahun() {
        return periodeTahun;
    }

    public void setPeriodeTahun(short periodeTahun) {
        this.periodeTahun = periodeTahun;
    }

    public Date getPeriodeTglawal() {
        return periodeTglawal;
    }

    public void setPeriodeTglawal(Date periodeTglawal) {
        this.periodeTglawal = periodeTglawal;
    }

    public Date getPeriodeTglakhir() {
        return periodeTglakhir;
    }

    public void setPeriodeTglakhir(Date periodeTglakhir) {
        this.periodeTglakhir = periodeTglakhir;
    }

    public String getPeriodeJenis() {
        return periodeJenis;
    }

    public void setPeriodeJenis(String periodeJenis) {
        this.periodeJenis = periodeJenis;
    }

    @XmlTransient
    public List<Jadwal> getJadwalList() {
        return jadwalList;
    }

    public void setJadwalList(List<Jadwal> jadwalList) {
        this.jadwalList = jadwalList;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (periodeId != null ? periodeId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Periode)) {
            return false;
        }
        Periode other = (Periode) object;
        if ((this.periodeId == null && other.periodeId != null) || (this.periodeId != null && !this.periodeId.equals(other.periodeId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.bbbe.Model.Periode[ periodeId=" + periodeId + " ]";
    }

    public int getPeriodeJumlahPembimbing() {
        return periodeJumlahPembimbing;
    }

    public void setPeriodeJumlahPembimbing(int periodeJumlahPembimbing) {
        this.periodeJumlahPembimbing = periodeJumlahPembimbing;
    }

    public int getPeriodeJumlahPenguji() {
        return periodeJumlahPenguji;
    }

    public void setPeriodeJumlahPenguji(int periodeJumlahPenguji) {
        this.periodeJumlahPenguji = periodeJumlahPenguji;
    }

    public Date getPeriodeTglbatas() {
        return periodeTglbatas;
    }

    public void setPeriodeTglbatas(Date periodeTglbatas) {
        this.periodeTglbatas = periodeTglbatas;
    }

    public TemplateJam getPeriodeTemplateJam() {
        return periodeTemplateJam;
    }

    public void setPeriodeTemplateJam(TemplateJam periodeTemplateJam) {
        this.periodeTemplateJam = periodeTemplateJam;
    }
/*
    public Integer getPeriodeJumlahPembimbing() {
        return periodeJumlahPembimbing;
    }
*/
    public void setPeriodeJumlahPembimbing(Integer periodeJumlahPembimbing) {
        this.periodeJumlahPembimbing = periodeJumlahPembimbing;
    }
    
}
