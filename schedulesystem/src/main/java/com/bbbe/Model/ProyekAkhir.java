/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.bbbe.Model;

import java.io.Serializable;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author SONY
 */
@Entity
@Table(name = "tbl_proyek_akhir")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "ProyekAkhir.findAll", query = "SELECT p FROM ProyekAkhir p"),
    @NamedQuery(name = "ProyekAkhir.findByProyekAkhirId", query = "SELECT p FROM ProyekAkhir p WHERE p.proyekAkhirId = :proyekAkhirId"),
    @NamedQuery(name = "ProyekAkhir.findByProyekAkhirJudul", query = "SELECT p FROM ProyekAkhir p WHERE p.proyekAkhirJudul = :proyekAkhirJudul"),
    @NamedQuery(name = "ProyekAkhir.findByProyekAkhirStatus", query = "SELECT p FROM ProyekAkhir p WHERE p.proyekAkhirStatus = :proyekAkhirStatus"),
    @NamedQuery(name = "ProyekAkhir.findByProyekAkhirTopik", query = "SELECT p FROM ProyekAkhir p WHERE p.proyekAkhirTopik = :proyekAkhirTopik")})
public class ProyekAkhir implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "proyek_akhir_id")
    private Integer proyekAkhirId;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 255)
    @Column(name = "proyek_akhir_judul")
    private String proyekAkhirJudul;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 255)
    @Column(name = "proyek_akhir_status")
    private String proyekAkhirStatus;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 255)
    @Column(name = "proyek_akhir_topik")
    private String proyekAkhirTopik;
    @JoinTable(name = "tbl_relasi_proyek_akhir_dosen_pembimbing", joinColumns = {
        @JoinColumn(name = "pembimbing_proyek_akhir", referencedColumnName = "proyek_akhir_id")}, inverseJoinColumns = {
        @JoinColumn(name = "pembimbing_dosen", referencedColumnName = "dosen_initial")})
    @ManyToMany
    private List<Dosen> dosenList;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "proyekAkhir")
    private List<RelasiProyekAkhirDosenPenguji> relasiProyekAkhirDosenPengujiList;
    @JoinColumn(name = "proyek_akhir_mahasiswa", referencedColumnName = "mhs_nim")
    @ManyToOne(optional = false)
    private Mahasiswa proyekAkhirMahasiswa;
    @JoinColumn(name = "proyek_akhir_jadwal", referencedColumnName = "jadwal_id")
    @ManyToOne(optional = false)
    private Jadwal proyekAkhirJadwal;

    public ProyekAkhir() {
    }

    public ProyekAkhir(Integer proyekAkhirId) {
        this.proyekAkhirId = proyekAkhirId;
    }

    public ProyekAkhir(Integer proyekAkhirId, String proyekAkhirJudul, String proyekAkhirStatus, String proyekAkhirTopik) {
        this.proyekAkhirId = proyekAkhirId;
        this.proyekAkhirJudul = proyekAkhirJudul;
        this.proyekAkhirStatus = proyekAkhirStatus;
        this.proyekAkhirTopik = proyekAkhirTopik;
    }

    public Integer getProyekAkhirId() {
        return proyekAkhirId;
    }

    public void setProyekAkhirId(Integer proyekAkhirId) {
        this.proyekAkhirId = proyekAkhirId;
    }

    public String getProyekAkhirJudul() {
        return proyekAkhirJudul;
    }

    public void setProyekAkhirJudul(String proyekAkhirJudul) {
        this.proyekAkhirJudul = proyekAkhirJudul;
    }

    public String getProyekAkhirStatus() {
        return proyekAkhirStatus;
    }

    public void setProyekAkhirStatus(String proyekAkhirStatus) {
        this.proyekAkhirStatus = proyekAkhirStatus;
    }

    public String getProyekAkhirTopik() {
        return proyekAkhirTopik;
    }

    public void setProyekAkhirTopik(String proyekAkhirTopik) {
        this.proyekAkhirTopik = proyekAkhirTopik;
    }

    @XmlTransient
    public List<Dosen> getDosenList() {
        return dosenList;
    }

    public void setDosenList(List<Dosen> dosenList) {
        this.dosenList = dosenList;
    }

    @XmlTransient
    public List<RelasiProyekAkhirDosenPenguji> getRelasiProyekAkhirDosenPengujiList() {
        return relasiProyekAkhirDosenPengujiList;
    }

    public void setRelasiProyekAkhirDosenPengujiList(List<RelasiProyekAkhirDosenPenguji> relasiProyekAkhirDosenPengujiList) {
        this.relasiProyekAkhirDosenPengujiList = relasiProyekAkhirDosenPengujiList;
    }

    public Mahasiswa getProyekAkhirMahasiswa() {
        return proyekAkhirMahasiswa;
    }

    public void setProyekAkhirMahasiswa(Mahasiswa proyekAkhirMahasiswa) {
        this.proyekAkhirMahasiswa = proyekAkhirMahasiswa;
    }

    public Jadwal getProyekAkhirJadwal() {
        return proyekAkhirJadwal;
    }

    public void setProyekAkhirJadwal(Jadwal proyekAkhirJadwal) {
        this.proyekAkhirJadwal = proyekAkhirJadwal;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (proyekAkhirId != null ? proyekAkhirId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof ProyekAkhir)) {
            return false;
        }
        ProyekAkhir other = (ProyekAkhir) object;
        if ((this.proyekAkhirId == null && other.proyekAkhirId != null) || (this.proyekAkhirId != null && !this.proyekAkhirId.equals(other.proyekAkhirId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.bbbe.Model.ProyekAkhir[ proyekAkhirId=" + proyekAkhirId + " ]";
    }
    
}
