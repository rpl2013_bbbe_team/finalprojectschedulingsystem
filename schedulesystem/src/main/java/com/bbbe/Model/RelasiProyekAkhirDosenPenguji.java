/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.bbbe.Model;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author SONY
 */
@Entity
@Table(name = "tbl_relasi_proyek_akhir_dosen_penguji")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "RelasiProyekAkhirDosenPenguji.findAll", query = "SELECT r FROM RelasiProyekAkhirDosenPenguji r"),
    @NamedQuery(name = "RelasiProyekAkhirDosenPenguji.findByPengujiProyekAkhir", query = "SELECT r FROM RelasiProyekAkhirDosenPenguji r WHERE r.relasiProyekAkhirDosenPengujiPK.pengujiProyekAkhir = :pengujiProyekAkhir"),
    @NamedQuery(name = "RelasiProyekAkhirDosenPenguji.findByPengujiDosen", query = "SELECT r FROM RelasiProyekAkhirDosenPenguji r WHERE r.relasiProyekAkhirDosenPengujiPK.pengujiDosen = :pengujiDosen"),
    @NamedQuery(name = "RelasiProyekAkhirDosenPenguji.findByPengujiStatus", query = "SELECT r FROM RelasiProyekAkhirDosenPenguji r WHERE r.pengujiStatus = :pengujiStatus")})
public class RelasiProyekAkhirDosenPenguji implements Serializable {
    private static final long serialVersionUID = 1L;
    @EmbeddedId
    protected RelasiProyekAkhirDosenPengujiPK relasiProyekAkhirDosenPengujiPK;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 255)
    @Column(name = "penguji_status")
    private String pengujiStatus;
    @JoinColumn(name = "penguji_dosen", referencedColumnName = "dosen_initial", insertable = false, updatable = false)
    @ManyToOne(optional = false)
    private Dosen dosen;
    @JoinColumn(name = "penguji_proyek_akhir", referencedColumnName = "proyek_akhir_id", insertable = false, updatable = false)
    @ManyToOne(optional = false)
    private ProyekAkhir proyekAkhir;

    public RelasiProyekAkhirDosenPenguji() {
    }

    public RelasiProyekAkhirDosenPenguji(RelasiProyekAkhirDosenPengujiPK relasiProyekAkhirDosenPengujiPK) {
        this.relasiProyekAkhirDosenPengujiPK = relasiProyekAkhirDosenPengujiPK;
    }

    public RelasiProyekAkhirDosenPenguji(RelasiProyekAkhirDosenPengujiPK relasiProyekAkhirDosenPengujiPK, String pengujiStatus) {
        this.relasiProyekAkhirDosenPengujiPK = relasiProyekAkhirDosenPengujiPK;
        this.pengujiStatus = pengujiStatus;
    }

    public RelasiProyekAkhirDosenPenguji(int pengujiProyekAkhir, String pengujiDosen) {
        this.relasiProyekAkhirDosenPengujiPK = new RelasiProyekAkhirDosenPengujiPK(pengujiProyekAkhir, pengujiDosen);
    }

    public RelasiProyekAkhirDosenPengujiPK getRelasiProyekAkhirDosenPengujiPK() {
        return relasiProyekAkhirDosenPengujiPK;
    }

    public void setRelasiProyekAkhirDosenPengujiPK(RelasiProyekAkhirDosenPengujiPK relasiProyekAkhirDosenPengujiPK) {
        this.relasiProyekAkhirDosenPengujiPK = relasiProyekAkhirDosenPengujiPK;
    }

    public String getPengujiStatus() {
        return pengujiStatus;
    }

    public void setPengujiStatus(String pengujiStatus) {
        this.pengujiStatus = pengujiStatus;
    }

    public Dosen getDosen() {
        return dosen;
    }

    public void setDosen(Dosen dosen) {
        this.dosen = dosen;
    }

    public ProyekAkhir getProyekAkhir() {
        return proyekAkhir;
    }

    public void setProyekAkhir(ProyekAkhir proyekAkhir) {
        this.proyekAkhir = proyekAkhir;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (relasiProyekAkhirDosenPengujiPK != null ? relasiProyekAkhirDosenPengujiPK.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof RelasiProyekAkhirDosenPenguji)) {
            return false;
        }
        RelasiProyekAkhirDosenPenguji other = (RelasiProyekAkhirDosenPenguji) object;
        if ((this.relasiProyekAkhirDosenPengujiPK == null && other.relasiProyekAkhirDosenPengujiPK != null) || (this.relasiProyekAkhirDosenPengujiPK != null && !this.relasiProyekAkhirDosenPengujiPK.equals(other.relasiProyekAkhirDosenPengujiPK))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.bbbe.Model.RelasiProyekAkhirDosenPenguji[ relasiProyekAkhirDosenPengujiPK=" + relasiProyekAkhirDosenPengujiPK + " ]";
    }
    
}
