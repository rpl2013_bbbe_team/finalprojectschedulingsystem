/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.bbbe.Model;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 *
 * @author SONY
 */
@Embeddable
public class RelasiProyekAkhirDosenPengujiPK implements Serializable {
    @Basic(optional = false)
    @NotNull
    @Column(name = "penguji_proyek_akhir")
    private int pengujiProyekAkhir;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 5)
    @Column(name = "penguji_dosen")
    private String pengujiDosen;

    public RelasiProyekAkhirDosenPengujiPK() {
    }

    public RelasiProyekAkhirDosenPengujiPK(int pengujiProyekAkhir, String pengujiDosen) {
        this.pengujiProyekAkhir = pengujiProyekAkhir;
        this.pengujiDosen = pengujiDosen;
    }

    public int getPengujiProyekAkhir() {
        return pengujiProyekAkhir;
    }

    public void setPengujiProyekAkhir(int pengujiProyekAkhir) {
        this.pengujiProyekAkhir = pengujiProyekAkhir;
    }

    public String getPengujiDosen() {
        return pengujiDosen;
    }

    public void setPengujiDosen(String pengujiDosen) {
        this.pengujiDosen = pengujiDosen;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (int) pengujiProyekAkhir;
        hash += (pengujiDosen != null ? pengujiDosen.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof RelasiProyekAkhirDosenPengujiPK)) {
            return false;
        }
        RelasiProyekAkhirDosenPengujiPK other = (RelasiProyekAkhirDosenPengujiPK) object;
        if (this.pengujiProyekAkhir != other.pengujiProyekAkhir) {
            return false;
        }
        if ((this.pengujiDosen == null && other.pengujiDosen != null) || (this.pengujiDosen != null && !this.pengujiDosen.equals(other.pengujiDosen))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.bbbe.Model.RelasiProyekAkhirDosenPengujiPK[ pengujiProyekAkhir=" + pengujiProyekAkhir + ", pengujiDosen=" + pengujiDosen + " ]";
    }
    
}
