/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.bbbe.Model;

import java.io.Serializable;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author SONY
 */
@Entity
@Table(name = "tbl_ruang")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Ruang.findAll", query = "SELECT r FROM Ruang r"),
    @NamedQuery(name = "Ruang.findByRuangId", query = "SELECT r FROM Ruang r WHERE r.ruangId = :ruangId"),
    @NamedQuery(name = "Ruang.findByRuangNama", query = "SELECT r FROM Ruang r WHERE r.ruangNama = :ruangNama"),
    @NamedQuery(name = "Ruang.findByRuangKapasitas", query = "SELECT r FROM Ruang r WHERE r.ruangKapasitas = :ruangKapasitas")})
public class Ruang implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "ruang_id")
    private Integer ruangId;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 255)
    @Column(name = "ruang_nama")
    private String ruangNama;
    @Basic(optional = false)
    @NotNull
    @Column(name = "ruang_kapasitas")
    private int ruangKapasitas;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "jadwalKuliahRuang")
    private List<JadwalMengajar> jadwalMengajarList;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "jadwalRuang")
    private List<Jadwal> jadwalList;

    public Ruang() {
    }

    public Ruang(Integer ruangId) {
        this.ruangId = ruangId;
    }

    public Ruang(Integer ruangId, String ruangNama, int ruangKapasitas) {
        this.ruangId = ruangId;
        this.ruangNama = ruangNama;
        this.ruangKapasitas = ruangKapasitas;
    }

    public Integer getRuangId() {
        return ruangId;
    }

    public void setRuangId(Integer ruangId) {
        this.ruangId = ruangId;
    }

    public String getRuangNama() {
        return ruangNama;
    }

    public void setRuangNama(String ruangNama) {
        this.ruangNama = ruangNama;
    }

    public int getRuangKapasitas() {
        return ruangKapasitas;
    }

    public void setRuangKapasitas(int ruangKapasitas) {
        this.ruangKapasitas = ruangKapasitas;
    }

    @XmlTransient
    public List<JadwalMengajar> getJadwalMengajarList() {
        return jadwalMengajarList;
    }

    public void setJadwalMengajarList(List<JadwalMengajar> jadwalMengajarList) {
        this.jadwalMengajarList = jadwalMengajarList;
    }

    @XmlTransient
    public List<Jadwal> getJadwalList() {
        return jadwalList;
    }

    public void setJadwalList(List<Jadwal> jadwalList) {
        this.jadwalList = jadwalList;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (ruangId != null ? ruangId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Ruang)) {
            return false;
        }
        Ruang other = (Ruang) object;
        if ((this.ruangId == null && other.ruangId != null) || (this.ruangId != null && !this.ruangId.equals(other.ruangId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.bbbe.Model.Ruang[ ruangId=" + ruangId + " ]";
    }
    
}
