/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.bbbe.Model;

import java.io.Serializable;
import java.util.Collection;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author SONY
 */
@Entity
@Table(name = "tbl_template_jam")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "TemplateJam.findAll", query = "SELECT t FROM TemplateJam t"),
    @NamedQuery(name = "TemplateJam.findByTemplateJamId", query = "SELECT t FROM TemplateJam t WHERE t.templateJamId = :templateJamId"),
    @NamedQuery(name = "TemplateJam.findByTemplateJamNama", query = "SELECT t FROM TemplateJam t WHERE t.templateJamNama = :templateJamNama"),
    @NamedQuery(name = "TemplateJam.findByTemplateJamAwal", query = "SELECT t FROM TemplateJam t WHERE t.templateJamAwal = :templateJamAwal"),
    @NamedQuery(name = "TemplateJam.findByTemplateJamAkhir", query = "SELECT t FROM TemplateJam t WHERE t.templateJamAkhir = :templateJamAkhir"),
    @NamedQuery(name = "TemplateJam.findByTemplateJamIstirahatAwal", query = "SELECT t FROM TemplateJam t WHERE t.templateJamIstirahatAwal = :templateJamIstirahatAwal"),
    @NamedQuery(name = "TemplateJam.findByTemplateJamIstirahatAkhir", query = "SELECT t FROM TemplateJam t WHERE t.templateJamIstirahatAkhir = :templateJamIstirahatAkhir"),
    @NamedQuery(name = "TemplateJam.findByTemplateJamLamaSlot", query = "SELECT t FROM TemplateJam t WHERE t.templateJamLamaSlot = :templateJamLamaSlot")})
public class TemplateJam implements Serializable {
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "periodeTemplateJam")
    private Collection<Periode> periodeCollection;
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "template_jam_id")
    private Integer templateJamId;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 255)
    @Column(name = "template_jam_nama")
    private String templateJamNama;
    @Basic(optional = false)
    @NotNull
    @Column(name = "template_jam_awal")
    @Temporal(TemporalType.TIME)
    private Date templateJamAwal;
    @Basic(optional = false)
    @NotNull
    @Column(name = "template_jam_akhir")
    @Temporal(TemporalType.TIME)
    private Date templateJamAkhir;
    @Basic(optional = false)
    @NotNull
    @Column(name = "template_jam_istirahat_awal")
    @Temporal(TemporalType.TIME)
    private Date templateJamIstirahatAwal;
    @Basic(optional = false)
    @NotNull
    @Column(name = "template_jam_istirahat_akhir")
    @Temporal(TemporalType.TIME)
    private Date templateJamIstirahatAkhir;
    @Basic(optional = false)
    @NotNull
    @Column(name = "template_jam_lama_slot")
    private int templateJamLamaSlot;

    public TemplateJam() {
    }

    public TemplateJam(Integer templateJamId) {
        this.templateJamId = templateJamId;
    }

    public TemplateJam(Integer templateJamId, String templateJamNama, Date templateJamAwal, Date templateJamAkhir, Date templateJamIstirahatAwal, Date templateJamIstirahatAkhir, int templateJamLamaSlot) {
        this.templateJamId = templateJamId;
        this.templateJamNama = templateJamNama;
        this.templateJamAwal = templateJamAwal;
        this.templateJamAkhir = templateJamAkhir;
        this.templateJamIstirahatAwal = templateJamIstirahatAwal;
        this.templateJamIstirahatAkhir = templateJamIstirahatAkhir;
        this.templateJamLamaSlot = templateJamLamaSlot;
    }

    public Integer getTemplateJamId() {
        return templateJamId;
    }

    public void setTemplateJamId(Integer templateJamId) {
        this.templateJamId = templateJamId;
    }

    public String getTemplateJamNama() {
        return templateJamNama;
    }

    public void setTemplateJamNama(String templateJamNama) {
        this.templateJamNama = templateJamNama;
    }

    public Date getTemplateJamAwal() {
        return templateJamAwal;
    }

    public void setTemplateJamAwal(Date templateJamAwal) {
        this.templateJamAwal = templateJamAwal;
    }

    public Date getTemplateJamAkhir() {
        return templateJamAkhir;
    }

    public void setTemplateJamAkhir(Date templateJamAkhir) {
        this.templateJamAkhir = templateJamAkhir;
    }

    public Date getTemplateJamIstirahatAwal() {
        return templateJamIstirahatAwal;
    }

    public void setTemplateJamIstirahatAwal(Date templateJamIstirahatAwal) {
        this.templateJamIstirahatAwal = templateJamIstirahatAwal;
    }

    public Date getTemplateJamIstirahatAkhir() {
        return templateJamIstirahatAkhir;
    }

    public void setTemplateJamIstirahatAkhir(Date templateJamIstirahatAkhir) {
        this.templateJamIstirahatAkhir = templateJamIstirahatAkhir;
    }

    public int getTemplateJamLamaSlot() {
        return templateJamLamaSlot;
    }

    public void setTemplateJamLamaSlot(int templateJamLamaSlot) {
        this.templateJamLamaSlot = templateJamLamaSlot;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (templateJamId != null ? templateJamId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof TemplateJam)) {
            return false;
        }
        TemplateJam other = (TemplateJam) object;
        if ((this.templateJamId == null && other.templateJamId != null) || (this.templateJamId != null && !this.templateJamId.equals(other.templateJamId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.bbbe.Model.TemplateJam[ templateJamId=" + templateJamId + " ]";
    }

    @XmlTransient
    public Collection<Periode> getPeriodeCollection() {
        return periodeCollection;
    }

    public void setPeriodeCollection(Collection<Periode> periodeCollection) {
        this.periodeCollection = periodeCollection;
    }
    
}
