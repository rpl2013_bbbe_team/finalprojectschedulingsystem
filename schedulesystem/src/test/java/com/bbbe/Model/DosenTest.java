/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.bbbe.Model;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author blasiusneri
 */
public class DosenTest {
    
    Dosen dosen;
    
    public DosenTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
        dosen = new Dosen();
    }
    
    @After
    public void tearDown() {
    }

    // TODO add test methods here.
    // The methods must be annotated with annotation @Test. For example:
    //
    @Test
    public void getDosenGelarBelakangTest() {
        assertNull("getDosenGelarBelakangTest",dosen.getDosenGelarBelakang());
    }
    
    @Test
    public void getDosenGelarDepanTest() {
        assertNull("getDosenGelarDepanTest",dosen.getDosenGelarDepan());
    }
    
    @Test
    public void getDosenInitialTest() {
        assertNull("getDosenInitialTest",dosen.getDosenInitial());
    }
    
    @Test
    public void getDosenKkTest() {
        assertNull("getDosenKkTest",dosen.getDosenKk());
    }
    
    @Test
    public void getDosenNamaTest() {
        assertNull("getDosenNamaTest",dosen.getDosenNama());
    }
}
